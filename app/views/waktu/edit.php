<?php if($this->session->has_userdata('waktuada')){ ?>
<script type="text/javascript">
  alert('Waktu sudah ada...')
</script>
<?php } ?>
<section class="content-header">
  <h1>
    Dashboard
    <small>Control panel</small>
  </h1>
  <ol class="breadcrumb">
    <li><a href="<?= site_url('') ?>"><i class="fa fa-dashboard"></i> Beranda</a></li>
    <li><a href="<?= site_url('waktu') ?>">Waktu</a></li>
    <li class="active">Edit</li>
  </ol>
</section>

<section class="content">
  <div class="row">
    <div class="col-sm-12">
      <div class="box box-primary">
        <div class="box-header">
          <h3 class="box-title">Edit Waktu</h3>
        </div>
        <?= form_open_multipart('waktu/update/'.$waktu['id_waktu']) ?>
        <div class="box-body">
          <div class="form-group">
            <label for="jenis_kegiatan">Jenis Kegiatan</label>
            <select class="form-control" name="jenis_kegiatan" id="jenis_kegiatan">
              <?php if (set_value('jenis_kegiatan')){ ?>
                <option <?= set_value('jenis_kegiatan') == "Kelas" ? "selected" : "" ?> value="Kelas">Kelas</option>
                <option <?= set_value('jenis_kegiatan') == "Kegiatan Lain" ? "selected" : "" ?> value="Kegiatan Lain">Kegiatan Lain</option>
              <?php }else{ ?>
                <option <?= $waktu['jenis_kegiatan'] == "Kelas" ? "selected" : "" ?> value="Kelas">Kelas</option>
                <option <?= $waktu['jenis_kegiatan'] == "Kegiatan Lain" ? "selected" : "" ?> value="Kegiatan Lain">Kegiatan Lain</option>
              <?php } ?>
            </select>
            <?= form_error('jenis_kegiatan', '<p class="alert alert-danger">', '</p>'); ?>
          </div>
          <div class="form-group">
            <label for="kegiatan">Kegiatan</label>
            <input type="text" name="kegiatan" value="<?= set_value('kegiatan')?set_value('kegiatan'):$waktu['kegiatan'] ?>" id="kegiatan" class="form-control" placeholder="contoh : Jam ke-1">
            <?= form_error('kegiatan', '<p class="alert alert-danger">', '</p>'); ?>
          </div>
          <div class="form-group">
            <label for="jenis_waktu">Jenis Waktu</label>
            <select class="form-control" name="jenis_waktu" id="jenis_waktu">
              <?php if (set_value('jenis_waktu')): ?>
                <option <?= set_value('jenis_waktu') == "Besar" ? "selected" : "" ?> value="Besar">Besar</option>
                <option <?= set_value('jenis_waktu') == "Kecil" ? "selected" : "" ?> value="Kecil">Kecil</option>
              <?php else: ?>
                <option <?= $waktu['jenis_waktu'] == "Besar" ? "selected" : "" ?> value="Besar">Besar</option>
                <option <?= $waktu['jenis_waktu'] == "Kecil" ? "selected" : "" ?> value="Kecil">Kecil</option>
              <?php endif; ?>
            </select>
            <?= form_error('jenis_waktu', '<p class="alert alert-danger">', '</p>'); ?>
          </div>
          <div class="row">
            <div class="col-sm-6">
              <label for="jam_mulai">Jam Mulai</label>
              <div class="input-group">
                <input type="text" class="form-control" id="jam_mulai" name="jam_mulai" value="<?= set_value('jam_mulai')?set_value('jam_mulai'):$waktu['jam_mulai'] ?>">
                <div class="input-group-addon">
                  <i class="fa fa-clock-o"></i>
                </div>
              </div>
              <?= form_error('jam_mulai', '<p class="alert alert-danger">', '</p>'); ?>
            </div>
            <div class="col-sm-6">
              <label for="jam_selesai">Jam Selesai</label>
              <div class="input-group">
                <input type="text" class="form-control" id="jam_selesai" name="jam_selesai" value="<?= set_value('jam_selesai')?set_value('jam_selesai'):$waktu['jam_selesai'] ?>">
                <div class="input-group-addon">
                  <i class="fa fa-clock-o"></i>
                </div>
              </div>
              <?= form_error('jam_selesai', '<p class="alert alert-danger">', '</p>'); ?>
            </div>
          </div>
        </div>
        <div class="box-footer">
          <a href="<?= site_url('waktu') ?>" class="btn btn-default">Kembali</a>
          <button type="submit" class="btn btn-primary">Submit</button>
        </div>
        <?= form_close() ?>
      </div>
    </div>
  </div>
</section>
<script type="text/javascript">
$(function(){
  $('#jam_mulai').timepicker({
    showInputs: false,
    showMeridian: false
  });
  $('#jam_selesai').timepicker({
    showInputs: false,
    showMeridian: false
  });
})
</script>
