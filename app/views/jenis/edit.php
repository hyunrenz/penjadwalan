
<section class="content-header">
  <h1>
    Dashboard
    <small>Control panel</small>
  </h1>
  <ol class="breadcrumb">
    <li><a href="<?= site_url('') ?>"><i class="fa fa-dashboard"></i> Beranda</a></li>
    <li><a href="<?= site_url('mapel') ?>"> Mata Pelajaran</a></li>
    <li><a href="<?= site_url('mapel/add') ?>"> Tambah Mata Pelajaran</a></li>
    <li><a href="<?= site_url('jenis') ?>"> Jenis Mata Pelajaran</a></li>
    <li class="active">Edit</li>
  </ol>
</section>

<section class="content">
  <div class="row">
    <div class="col-sm-12">
      <div class="box box-primary">
        <div class="box-header">
          <h3 class="box-title">Edit Jenis Mata Pelajaran</h3>
        </div>
        <div class="box-body">
          <?= form_open('jenis/update/'.$jenis['id_jenismapel']) ?>
          <div class="form-group">
            <label for="jenis_mapel">Jenis Mata Pelajaran</label>
            <input id="jenis_mapel" class="form-control" type="text" name="jenis_mapel" value="<?= set_value('jenis_mapel')?set_value('jenis_mapel'):$jenis['jenis_mapel'] ?>" placeholder="Misal : Muatan Nasional">
            <?= form_error('jenis_mapel','<p class="alert alert-danger">','</p>') ?>
          </div>
          <button class="btn btn-primary" type="submit">Submit</button>
          <?= form_close() ?>
        </div>
      </div>
    </div>
  </div>
</section>
