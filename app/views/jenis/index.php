
<section class="content-header">
  <h1>
    Dashboard
    <small>Control panel</small>
  </h1>
  <ol class="breadcrumb">
    <li><a href="<?= site_url('') ?>"><i class="fa fa-dashboard"></i> Beranda</a></li>
    <li><a href="<?= site_url('mapel') ?>"> Mata Pelajaran</a></li>
    <li><a href="<?= site_url('mapel/add') ?>"> Tambah Mata Pelajaran</a></li>
    <li class="active">Jenis Mata Pelajaran</li>
  </ol>
</section>

<section class="content">
  <div class="row">
    <div class="col-sm-12">
      <div class="box box-primary">
        <div class="box-header">
          <h3 class="box-title">Tambah Jenis Mata Pelajaran</h3>
        </div>
        <div class="box-body">
          <a href="<?= site_url('mapel/add') ?>" class="btn btn-default">Kembali</a>
        </div>
        <div class="box-body">
          <?= form_open('jenis/store') ?>
          <div class="form-group">
            <label for="jenis_mapel">Jenis Mata Pelajaran</label>
            <input id="jenis_mapel" class="form-control" type="text" name="jenis_mapel" value="<?= set_value('jenis_mapel') ?>" placeholder="Misal : Muatan Nasional">
            <?= form_error('jenis_mapel','<p class="alert alert-danger">','</p>') ?>
          </div>
          <button class="btn btn-primary" type="submit">Submit</button>
          <?= form_close() ?>
        </div>
        <div class="box-body">
          <h4>Data Jenis Mata Pelajaran</h4>
          <table class="table table-bordered table-striped" style="width:100%">
            <thead>
              <tr>
                <th>Jenis Mata Pelajaran</th>
                <th>Status</th>
                <th>Aksi</th>
              </tr>
            </thead>
            <tbody>
              <?php foreach ($jenis as $j): ?>
                <tr>
                  <td><?= $j['jenis_mapel'] ?></td>
                  <td><?= $j['status'] == '1' ? "Aktif" : "Tidak Aktif" ?></td>
                  <td>
                    <a class="btn btn-success" href="<?= site_url('jenis/edit/'.$j['id_jenismapel']) ?>">
                      <i class="fa fa-edit"></i>
                    </a>
                    <a class="btn btn-warning" href="<?= site_url('jenis/update_status/'.$j['id_jenismapel']) ?>">
                      <i class="fa fa-eye"></i>
                    </a>
                  </td>
                </tr>
              <?php endforeach; ?>
            </tbody>
          </table>
        </div>
      </div>
    </div>
  </div>
</section>
