<?php $uri = $this->uri->segment(1) ?>
<?php $jabatan = $this->session->userdata('user')['jabatan']; ?>
<!-- sidebar: style can be found in sidebar.less -->
<section class="sidebar">
	<!-- Sidebar user panel -->
	<div class="user-panel">
		<div class="pull-left image">
			<img src="<?= $assets ?>/foto/<?= $this->session->userdata('user')['foto'] ?>" style="width:36px;height:36px" class="img-circle" alt="User Image">
		</div>
		<div class="pull-left info">
			<p><?= $this->session->userdata('user')['nama'] ?></p>
			<a href="#"><i class="fa fa-circle text-success"></i> Online</a>
		</div>
	</div>
	<!-- search form -->
	<form action="#" method="get" class="sidebar-form">
		<div class="input-group">
			<input type="text" name="q" class="form-control" placeholder="Search...">
			<span class="input-group-btn">
				<button type="submit" name="search" id="search-btn" class="btn btn-flat"><i class="fa fa-search"></i>
				</button>
			</span>
		</div>
	</form>
	<!-- /.search form -->
	<!-- sidebar menu: : style can be found in sidebar.less -->
	<ul class="sidebar-menu" data-widget="tree">
		<li class="header">MAIN NAVIGATION</li>
		<li class="<?= $uri == "pengumuman" ? "active" : "" ?> treeview">
			<a href="#">
				<i class="fa fa-dashboard"></i> <span>Beranda</span>
				<span class="pull-right-container">
					<i class="fa fa-angle-left pull-right"></i>
				</span>
			</a>
			<ul class="treeview-menu">
				<li class="<?= $uri == "pengumuman" ? "active" : "" ?>"><a href="<?= site_url('pengumuman') ?>"><i
					class="fa fa-circle-o"></i> Beranda</a></li>
				</ul>
			</li>
			<?php if($jabatan == "Waka Kurikulum"){ ?>
				<li  class="<?= $uri == "guru" || $uri == "kelas" || $uri == "mapel" || $uri == "waktu" ? "active" : "" ?> treeview">
					<a href="#">
						<i class="fa fa-files-o"></i> <span>Data</span>
						<span class="pull-right-container">
							<i class="fa fa-angle-left pull-right"></i>
						</span>
					</a>
					<ul class="treeview-menu">
						<li class="<?= $uri == "guru" ? "active" : "" ?>">
							<a href="<?= site_url('guru') ?>"><i class="fa fa-circle-o"></i> Guru</a>
						</li>
						<li class="<?= $uri == "kelas" ? "active" : "" ?>">
							<a href="<?= site_url('kelas') ?>"><i class="fa fa-circle-o"></i> Kelas</a>
						</li>
						<li class="<?= $uri == "mapel" ? "active" : "" ?>">
							<a href="<?= site_url('mapel') ?>"><i class="fa fa-circle-o"></i> Mata Pelajaran</a>
						</li>
						<li class="<?= $uri == "waktu" ? "active" : "" ?>">
							<a href="<?= site_url('waktu') ?>"><i class="fa fa-circle-o"></i> Waktu</a>
						</li>
					</ul>
				</li>
			<?php } ?>
			<li class="<?= $uri == "jadwal" || $uri == "pengajar" ? "active" : "" ?> treeview">
				<a href="#">
					<i class="fa fa-calendar"></i> <span>Jadwal</span>
					<span class="pull-right-container">
						<i class="fa fa-angle-left pull-right"></i>
					</span>
				</a>
				<ul class="treeview-menu">
					<?php if($jabatan == "Waka Kurikulum"){ ?>
						<!-- <li class="<?= $uri == "pengajar" ? "active" : "" ?>"> -->
						<!-- <a href="<?= site_url('pengajar') ?>"><i class="fa fa-circle-o"></i> Kelola Pengajar</a> -->
						<!-- </li> -->
						<li class="<?= $uri == "jadwal" ? "active" : "" ?>">
							<a href="<?= site_url('jadwal') ?>"><i class="fa fa-circle-o"></i> Kelola Jadwal</a>
						</li>
					<?php }else{ ?>
						<li class="<?= $uri == "jadwal" ? "active" : "" ?>">
							<a href="<?= site_url('jadwal/jadwalguru') ?>"><i class="fa fa-circle-o"></i> Lihat Jadwal</a>
						</li>
					<?php } ?>
				</ul>
			</li>
			<li class="<?= $uri == "profil" ? "active" : "" ?> treeview">
				<a href="#">
					<i class="fa fa-calendar"></i> <span>Profil</span>
					<span class="pull-right-container">
						<i class="fa fa-angle-left pull-right"></i>
					</span>
				</a>
				<ul class="treeview-menu">

					<li class="<?= $uri == "profil" ? "active" : "" ?>">
						<a href="<?= site_url('profil') ?>"><i class="fa fa-circle-o"></i> Lihat Profil Saya</a>
					</li>
				</ul>
			</li>
		</ul>
	</section>
	<!-- /.sidebar -->
