<?php $assets = base_url('assets') ?>
<!DOCTYPE html>
<html>
<head>
  <?php $this->load->view('layout/head',['assets'=>$assets]) ?>
</head>
	<body class="hold-transition skin-blue-light sidebar-mini">
<div class="wrapper">

  <header class="main-header">
    <?php $this->load->view('layout/header',['assets'=>$assets]) ?>
  </header>
  <!-- Left side column. contains the logo and sidebar -->
  <aside class="main-sidebar">
    <?php $this->load->view('layout/sidebar',['assets'=>$assets]); ?>
  </aside>

  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <?php if (!empty($_view)) {
       $this->load->view($_view);
    } ?>
  </div>
  <!-- /.content-wrapper -->
  <footer class="main-footer">
    <div class="pull-right hidden-xs">
      <b>Version</b> 2.4.0
    </div>
    <strong>Copyright &copy; 2014-2016 <a href="https://adminlte.io">Almsaeed Studio</a>.</strong> All rights
    reserved.
  </footer>

  <!-- Control Sidebar -->
  <aside class="control-sidebar control-sidebar-dark">
    <?php //$this->load->view('layout/setting',['assets'=>$assets]) ?>
  </aside>

  <div class="control-sidebar-bg"></div>
  <!-- /.control-sidebar -->

</div>

<?php $this->load->view('layout/script',['assets'=>$assets]) ?>

</body>
</html>
