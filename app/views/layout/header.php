
<!-- Logo -->
<a href="#" class="logo" style="padding:0;line-height:0">
  <!-- mini logo for sidebar mini 50x50 pixels -->
  <div class="logo-mini" style="width:100%;height:100%;margin-left:0">
    <img src="<?= $assets ?>/logomini.png" height="100%" width="100%" >
  </div>
  <!-- logo for regular state and mobile devices -->
  <div class="logo-lg" style="width:100%;height:100%">
    <img src="<?= $assets ?>/logo.jpg" height="100%" width="100%">
  </div>

  <!-- mini logo for sidebar mini 50x50 pixels -->
  <!-- <span class="logo-mini"><b>A</b>LT</span> -->
  <!-- logo for regular state and mobile devices -->
  <!-- <span class="logo-lg"><b>Admin</b>LTE</span> -->
</a>
<!-- Header Navbar: style can be found in header.less -->
<nav class="navbar navbar-static-top">
  <!-- Sidebar toggle button-->
  <a href="#" class="sidebar-toggle" data-toggle="push-menu" role="button">
    <span class="sr-only">Toggle navigation</span>
  </a>

  <div class="navbar-custom-menu">
    <ul class="nav navbar-nav">
      <li class="dropdown user user-menu">
        <a href="#" class="dropdown-toggle" data-toggle="dropdown">
          <img src="<?= $assets ?>/foto/<?= $this->session->userdata('user')['foto'] ?>"
			   class="user-image" alt="User Image">
          <span class="hidden-xs"><?= $this->session->userdata('user')['nama'] ?></span>
        </a>
        <ul class="dropdown-menu">
          <li class="user-header">
            <img src="<?= $assets ?>/foto/<?= $this->session->userdata('user')['foto'] ?>" class="img-circle" alt="User Image">
            <p>
              <?= $this->session->userdata('user')['nama'] ?>
            </p>
          </li>
          <li class="user-footer">
            <a href="<?= site_url('auth/logout') ?>" class="btn btn-default btn-flat" style="width:100%">Sign out</a>
          </li>
        </ul>
      </li>
    </ul>
  </div>
</nav>
