<?php $assets = base_url('assets') ?>
<!DOCTYPE html>
<html>
<head>
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<title>AdminLTE 2 | Log in</title>
	<!-- Tell the browser to be responsive to screen width -->
	<meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
	<!-- Bootstrap 3.3.7 -->
	<link rel="stylesheet" href="<?= $assets ?>/bower_components/bootstrap/dist/css/bootstrap.min.css">
	<!-- Font Awesome -->
	<link rel="stylesheet" href="<?= $assets ?>/bower_components/font-awesome/css/font-awesome.min.css">
	<!-- Ionicons -->
	<link rel="stylesheet" href="<?= $assets ?>/bower_components/Ionicons/css/ionicons.min.css">
	<!-- Theme style -->
	<link rel="stylesheet" href="<?= $assets ?>/dist/css/AdminLTE.min.css">
	<!-- iCheck -->
	<link rel="stylesheet" href="<?= $assets ?>/plugins/iCheck/square/blue.css">
	<link rel="stylesheet"
		  href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,600,700,300italic,400italic,600italic">
</head>
<body class="hold-transition login-page">
<div class="login-box">

	<div class="login-box-body">
			<img src="<?= $assets ?>/logo.png" width="100%">
		<h3 align="center"><strong>Login</strong></h3>
		<?= form_open('auth/login') ?>
		<div class="form-group has-feedback">
			<input name="username" type="text" class="form-control" placeholder="Masukkan username...">
			<span class="glyphicon glyphicon-user form-control-feedback"></span>
		</div>
		<div class="form-group has-feedback">
			<input name="password" type="password" class="form-control" placeholder="Masukkan password...">
			<span class="glyphicon glyphicon-lock form-control-feedback"></span>
		</div>
		<div class="row">
			<div class="col-xs-12">
				<?php if ($this->session->flashdata('auth') === FALSE) { ?>
					<p class="text-danger">Username atau password salah.</p>
				<?php } ?>
				<button type="submit" style="width:100%" class="btn btn-primary btn-block btn-flat">Sign In</button>
			</div>
		</div>
		</form>
	</div>
</div>

</body>
</html>
<!-- jQuery 3 -->
<script src="<?= $assets ?>/bower_components/jquery/dist/jquery.min.js"></script>
<!-- Bootstrap 3.3.7 -->
<script src="<?= $assets ?>/bower_components/bootstrap/dist/js/bootstrap.min.js"></script>
<!-- iCheck -->
<script src="<?= $assets ?>/plugins/iCheck/icheck.min.js"></script>
</script>
