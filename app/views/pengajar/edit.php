<section class="content-header">
  <h1>
    Dashboard
    <small>Control panel</small>
  </h1>
  <ol class="breadcrumb">
    <li><a href="<?= site_url('') ?>"><i class="fa fa-dashboard"></i> Beranda</a></li>
    <li><a href="<?= site_url('pengajar') ?>">Pengajar</a></li>
    <li class="active">Edit</li>
  </ol>
</section>

<section class="content">
  <div class="row">
    <div class="col-sm-12">
      <div class="box box-primary">
        <div class="box-header">
          <h3 class="box-title">Edit Pengajar</h3>
        </div>
        <?= form_open_multipart('pengajar/update/'.$pengajar['id_pengajar']) ?>
        <div class="box-body">
          <div class="form-group">
            <label for="nip">Pengajar</label>
            <select id="nip" class="form-control" name="nip">
              <?php foreach ($guru as $g): ?>
                <?php if(set_value('nip')){ ?>
                  <option <?= set_value('nip') == $g['nip'] ? "selected" : "" ?> value="<?= $g['nip'] ?>"><?= $g['nama'] ?> - <?= $g['nip'] ?></option>
                <?php }else{ ?>
                  <option <?= $pengajar['nip'] == $g['nip'] ? "selected" : "" ?> value="<?= $g['nip'] ?>"><?= $g['nama'] ?> - <?= $g['nip'] ?></option>
                <?php } ?>
              <?php endforeach; ?>
            </select>
            <?= form_error('nip', '<p class="alert alert-danger">', '</p>'); ?>
          </div>
          <div class="form-group">
            <label for="id_mapel">Mata Pelajaran</label>
            <select class="form-control" name="id_mapel" id="id_mapel">
              <?php foreach ($mapel as $m): ?>
                <?php if(set_value('id_mapel')){ ?>
                  <option <?= set_value('id_mapel') == $m['id_mapel'] ?> value="<?= $m['id_mapel'] ?>"><?= $m['nama_mapel'] ?></option>
                <?php }else{ ?>
                  <option <?= $m['id_mapel'] == $pengajar['id_mapel'] ?> value="<?= $m['id_mapel'] ?>"><?= $m['nama_mapel'] ?></option>
                <?php } ?>
              <?php endforeach; ?>
            </select>
            <?= form_error('id_mapel', '<p class="alert alert-danger">', '</p>'); ?>
          </div>
        </div>
        <div class="box-footer">
          <a href="<?= site_url('pengajar') ?>" class="btn btn-default">Kembali</a>
          <button type="submit" class="btn btn-primary">Submit</button>
        </div>
        <?= form_close() ?>
      </div>
    </div>
  </div>
</section>
