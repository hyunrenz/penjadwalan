
<section class="content-header">
  <h1>
    Dashboard
    <small>Control panel</small>
  </h1>
  <ol class="breadcrumb">
    <li><a href="<?= site_url('') ?>"><i class="fa fa-dashboard"></i> Beranda</a></li>
    <li class="active">Pengajar</li>
  </ol>
</section>

<section class="content">
  <div class="row">
    <div class="col-sm-12">
      <div class="box box-default">
        <div class="box-header">
          <h3 class="box-title">Data Pengajar</h3>
        </div>
        <div class="box-header">
          <a href="<?= site_url('pengajar/add') ?>" class="btn btn-success">
            <i class="fa fa-plus"></i> Tambah Pengajar
          </a>
          <a href="" class="btn btn-primary">
            <i class="fa fa-print"></i>
          </a>
        </div>
        <div class="box-body">
          <table class="table table-bordered table-striped" id="tabelpengajar" style="width:100%">
            <thead>
              <tr>
                <th>Nama</th>
                <th>Mata Pelajaran</th>
                <th>Jenis Mata Pelajaran</th>
                <th>Aksi</th>
              </tr>
            </thead>
            <tbody>
              <?php foreach ($pengajar as $k): ?>
                <tr>
                  <td><?= $k['nama'] ?></td>
                  <td><?= $k['nama_mapel'] ?></td>
                  <td><?= $k['jenis_mapel'] ?></td>
                  <td>
                    <a href="<?= site_url('pengajar/edit/'.$k['id_pengajar']) ?>" class="btn btn-primary">
                      <i class="fa fa-edit"></i>
                    </a>
                    <a href="<?= site_url('pengajar/update_status/'.$k['id_pengajar']) ?>" class="btn btn-danger">
                      <i class="fa fa-trash"></i>
                    </a>
                  </td>
                </tr>
              <?php endforeach; ?>
            </tbody>
          </table>
        </div>
      </div>
    </div>
  </div>
</section>

<script type="text/javascript">
$(function () {
  $('#tabelpengajar').DataTable()
})
</script>
