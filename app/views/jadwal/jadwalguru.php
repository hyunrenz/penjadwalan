<!-- JADWAL -->
<!--  <//?php $jabatan = $this->session->userdata('user')['jabatan']; if($jabatan != 'Waka Kurikulum'){?>style="display: none;" <//?php }?> -->

<section class="content-header">
  <h1>
    Dashboard
    <small>Control panel</small>
  </h1>
  <ol class="breadcrumb">
    <li><a href="<?= site_url('') ?>"><i class="fa fa-dashboard"></i> Beranda</a></li>
    <li class="active">Jadwal</li>
  </ol>
</section>

<section class="content" >
  <div class="row">
    <div class="col-sm-12">
      <div class="box box-default">
        <div class="box-header">
          <h3 class="box-title">Data Jadwal</h3>
        </div>
        <div class="box-header">
          <?php if(empty($jadwal)){ ?>
            <a class="btn btn-primary disabled"><i class="fa fa-print"></i></a>
          <?php }else{ ?>
            <?= form_open('jadwal/cetak_jadwal_guru/'.uuid()) ?>
            <input type="hidden" name="tahun" value="<?= $tahun ?>">
            <button type="submit" name="button" class="btn btn-primary"><i class="fa fa-print"></i></button>
            <?= form_close() ?>
          <?php } ?>
        </div>
        <?= form_open('jadwal/jadwalguru') ?>
        <div class="col-sm-12" style="margin-bottom: 30px; padding: 10px; align-content: center">
          <div class="col-sm-8"></div>
          <div class="col-sm-2">
            <label>Tahun</label>
            <select name="tahun">
              <?php foreach ($listtahun as $t) { ?>
                <option <?= $t['tahun'] == $tahun ? "selected" : "" ?> value="<?= $t['tahun']  ?>"><?= $t['tahun'] ?></option>
              <?php } ?>
            </select>
          </div>
          <div class="col-sm-2">
            <a>
              <button class="btn btn-success">Cek</button>
            </a>
          </div>
        </div>
        <?= form_close() ?>
        <div class="box-body">
          <table class="table table-bordered table-striped" id="tabeljadwal" style="width:100%">
            <thead>
              <tr>
                <th>Hari</th>
                <th>Kelas</th>
                <th>Mata Pelajaran</th>
                <th>Jam Mulai</th>
                <th>Jam Selesai</th>
              </tr>
            </thead>
            <?php if(empty($jadwal)){ ?>
              <tbody>
                <tr>
                  <td colspan="7" align="center">Belum ada jadwal...</td>
                </tr>
              </tbody>
            <?php }else{ ?>
              <tbody>
                <?php foreach ($jadwal as $j): ?>
                  <tr>
                    <td><?= $j['hari'] ?></td>
                    <td><?= $j['kelas'] ?></td>
                    <td><?= $j['nama_mapel'] ?></td>
                    <td><?= $j['jam_mulai'] ?></td>
                    <td><?= $j['jam_selesai'] ?></td>
                  </tr>
                <?php endforeach; ?>
              </tbody>
            <?php } ?>
          </table>
        </div>
      </div>
    </div>
  </div>
</section>
<div class="modal fade" id="modal-danger">
  <div class="modal-dialog modal-sm">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
        <h4 class="modal-title">Konfirmasi Box</h4>
      </div>
      <div class="modal-body">
        <p>Apakah anda yakin ingin menghapus&hellip;</p>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Tidak</button>
        <a id="hapus" href="" class="btn btn-danger">Ya</a>
      </div>
    </div>
  </div>
</div>

<script type="text/javascript">
$(function () {
  $('#tabelkelas').DataTable()
})
function hapus(id){
  $(function () {
    $('#hapus').attr('href','<?= site_url('jadwal/update_status/') ?>'+id)
    $('#modal-danger').modal();
  })
}
</script>
