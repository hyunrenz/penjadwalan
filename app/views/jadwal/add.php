<?php if($this->session->has_userdata('jadwalada')){ ?>
  <script type="text/javascript">
  alert('Jadwal sudah ada...')
</script>
<?php } ?>

<section class="content-header">
  <h1>
    Dashboard
    <small>Control panel</small>
  </h1>
  <ol class="breadcrumb">
    <li><a href="<?= site_url('') ?>"><i class="fa fa-dashboard"></i> Beranda</a></li>
    <li><a href="<?= site_url('jadwal') ?>">Jadwal</a></li>
    <li class="active">Tambah</li>
  </ol>
</section>

<section class="content">
  <div class="row">
    <div class="col-sm-12">
      <div class="box box-primary">
        <div class="box-header">
          <h3 class="box-title">Tambah Jadwal</h3>
        </div>
        <?= form_open_multipart('jadwal/store') ?>
        <div class="box-body">
          <div class="form-group">
            <input type="radio" value="Besar" id="besar" name="jenis"> Besar
            <input type="radio" value="Kecil" id="kecil" name="jenis"> Kecil
          </div>
          <div class="form-group">
            <label for="id_kelas">Kelas</label>
            <select class="form-control" id="id_kelas" name="id_kelas" disabled>
              <option value=""></option>
              <?php foreach ($kelas as $k): ?>
                <option <?= set_value('id_kelas') == $k['id_kelas']?"selected":"" ?> value="<?= $k['id_kelas'] ?>"><?= $k['tahun'] ?> - <?= $k['kelas'] ?></option>
              <?php endforeach; ?>
            </select>
            <?= form_error('id_kelas', '<p class="alert alert-danger">', '</p>'); ?>
          </div>
          <div class="form-group">
            <label for="hari">Hari</label>
            <select class="form-control" id="hari" name="hari" disabled>
              <option value=""></option>
              <option <?= set_value('hari') == "Senin" ? "selected" : "" ?> value="Senin">Senin</option>
              <option <?= set_value('hari') == "Selasa" ? "selected" : "" ?> value="Selasa">Selasa</option>
              <option <?= set_value('hari') == "Rabu" ? "selected" : "" ?> value="Rabu">Rabu</option>
              <option <?= set_value('hari') == "Kamis" ? "selected" : "" ?> value="Kamis">Kamis</option>
              <option <?= set_value('hari') == "Jumat" ? "selected" : "" ?> value="Jumat">Jumat</option>
            </select>
            <?= form_error('hari', '<p class="alert alert-danger">', '</p>'); ?>
          </div>
          <div class="form-group">
            <label for="id_waktu">Waktu</label>
            <select class="form-control" id="id_waktu" name="id_waktu" disabled>
              <option value=""></option>
              <?php foreach ($waktu as $m): ?>
                <option <?= set_value('id_waktu') == $m['id_waktu']?"selected":"" ?> value="<?= $m['id_waktu'] ?>"><?= $m['kegiatan'] ?> -- <?= $m['jam_mulai'] ?> - <?= $m['jam_selesai'] ?></option>
              <?php endforeach; ?>
            </select>
            <?= form_error('id_waktu', '<p class="alert alert-danger">', '</p>'); ?>
          </div>
          <div class="form-group">
            <label for="nip">Pengajar</label>
            <select class="form-control" id="nip" name="nip" disabled>
              <option value=""></option>
              <?php foreach ($guru as $g): ?>
                <option <?= set_value('nip') == $g['nip']?"selected":"" ?> value="<?= $g['nip'] ?>"><?= $g['nama'] ?> - <?= $g['nip'] ?></option>
              <?php endforeach; ?>
            </select>
            <?= form_error('nip', '<p class="alert alert-danger">', '</p>'); ?>
          </div>
          <div class="form-group">
            <label for="id_mapel">Mata Pelajaran</label>
            <select class="form-control" id="id_mapel" name="id_mapel" disabled>
              <option value=""></option>
              <?php foreach ($mapel as $m): ?>
                <option <?= set_value('id_mapel') == $m['id_mapel']?"selected":"" ?> value="<?= $m['id_mapel'] ?>"><?= $m['nama_mapel'] ?> - <?= $m['jenis_mapel'] ?></option>
              <?php endforeach; ?>
            </select>
            <?= form_error('id_mapel', '<p class="alert alert-danger">', '</p>'); ?>
          </div>
        </div>
        <div class="box-footer">
          <a href="<?= site_url('jadwal') ?>" class="btn btn-default">Kembali</a>
          <button type="submit" class="btn btn-primary">Submit</button>
          <br><br>
          <table class="table table-striped table-bordered" id="jadwalkelas">
            <thead>
              <tr>
                <th>Hari</th>
                <th>Jam Ke</th>
                <th>Jam Mulai</th>
                <th>Jam Selesai</th>
                <th>Mata Pelajaran</th>
                <th>Kelas</th>
                <th>Pengajar</th>
              </tr>
            </thead>
            <tbody></tbody>
          </table>
        </div>
        <?= form_close() ?>
      </div>
    </div>
  </div>
</section>
<script type="text/javascript">
$(function(){
  $('#tahun_ajaran').datepicker({
    autoclose: true,
    format: "yyyy",
    viewMode: "years",
    minViewMode: "years"
  });
  $('[name="jenis"]').on('click',function(){
    $.ajax({
      "url" : "<?= site_url('jadwal') ?>"+"/req_kelas/"+$(this).val(),
      "type": "GET",
      "success" : function(result){
        var select_kelas = $("#id_kelas");
        select_kelas.html("<option value=''></option>");
        $.each(result.data,function(i,val){
          var pilihan = "<option value='"+val.id_kelas+"'>"+val.tahun+" "+val.kelas+"</option>";
          select_kelas.append(pilihan);
        });
      }
    });
    $.ajax({
      "url" : "<?= site_url('jadwal') ?>"+"/req_waktu/"+$(this).val(),
      "type": "GET",
      "success" : function(result){
        var select_waktu = $("#id_waktu");
        select_waktu.html("<option value=''></option>");
        $.each(result.data,function(i,val){
          var pilihan = "<option value='"+val.id_waktu+"'>"+val.kegiatan+" -- "+val.jam_mulai+" - "+val.jam_selesai+"</option>";
          select_waktu.append(pilihan);
        });
      }
    });
    $('#id_kelas').removeAttr('disabled');
    $('#hari').removeAttr('disabled');
    $('#id_waktu').removeAttr('disabled');
    $('#jadwalkelas tbody').html('');
  });
  $('#id_kelas').on('change',function(){
    $.ajax({
      "url" : "<?= site_url('jadwal') ?>"+"/req_jadwal/"+$(this).val(),
      "type": "GET",
      "success" : function(result){
        var jadwal_kelas = $('#jadwalkelas tbody');
        jadwal_kelas.html('');
        if(result.data.length > 0){
          $.each(result.data,function(i,val){
            var row = "<tr>"+
            "<td>"+val.hari+"</td>"+
            "<td>"+val.kegiatan+"</td>"+
            "<td>"+val.jam_mulai+"</td>"+
            "<td>"+val.jam_selesai+"</td>"+
            "<td>"+(val.nama_mapel?val.nama_mapel:"-")+"</td>"+
            "<td>"+val.kelas+"</td>"+
            "<td>"+(val.nama?val.nama:"-")+"</td>"+
            "</tr>";
            jadwal_kelas.append(row);
          });
        }else{
          var row = "<tr>"+
          "<td colspan='7' align='center'>Belum ada jadwal...</td>"+
          "</tr>";
          jadwal_kelas.append(row);
        }
      }
    });
  })
  $('#id_waktu').on('change',function(){
    $.ajax({
      "url" : "<?= site_url('jadwal') ?>"+"/req_jenis_waktu/"+$(this).val(),
      "type": "GET",
      "success" : function(result){
        if(result.data.jenis_kegiatan != "Kelas"){
          $('#nip').attr('disabled',true);
          $('#id_mapel').attr('disabled',true);
        }else{
          $('#nip').removeAttr('disabled');
          $('#id_mapel').removeAttr('disabled');
        }
      }
    });
  });
});
</script>
