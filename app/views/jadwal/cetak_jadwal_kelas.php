<?php
$hari = [
  'Senin','Selasa','Rabu','Kamis','Jumat',
];
?>
<!DOCTYPE html>
<html lang="en" dir="ltr">
<head>
  <meta charset="utf-8">
  <title></title>
  <style media="screen">
  *{
    margin:0;
  }
  </style>
</head>
<body>
  <table>
    <tr>
      <td style="width:10%"><img style="width:75px" src="<?= site_url('assets/logo.png') ?>" alt=""></td>
      <td align="center"><h2 style="font-size:20px">Jadwal Kelas <?= $kelas['kelas'] ?> Tahun <?= $kelas['tahun'] ?></h2></td>
    </tr>
  </table>
  <table class="table" border="1" style="width:100%;position:relative;top:70px">
    <thead>
      <tr>
        <th width="75px">Jam Ke</th>
        <th width="150px">Jam</th>
        <th>Senin</th>
        <th>Selasa</th>
        <th>Rabu</th>
        <th>Kamis</th>
        <th>Jumat</th>
      </tr>
    </thead>
    <tbody>
      <?php foreach ($waktu as $w): ?>
        <?php if($w['jenis_kegiatan'] == "Kelas"): ?>
          <tr>
            <td><?= $w['kegiatan'] ?></td>
            <td><?= $w['jam_mulai'] ?> - <?= $w['jam_selesai'] ?></td>
            <?php foreach ($hari as $h): ?>
              <td>
                <?php $i = 1 ?>
                <?php foreach ($jadwal as $j): ?>
                  <?php if ($j['hari'] == $h && $j['id_waktu'] == $w['id_waktu']): ?>
                    <?= $j['nama_mapel'] ?>
                    <br>
                    <?= $j['nama_pengajar'] ?>
                    <?php break; ?>
                  <?php else: ?>
                    <?php if ($i == count($jadwal)): ?>
                      -
                    <?php endif; ?>
                  <?php endif; ?>
                  <?php $i++ ?>
                <?php endforeach; ?>
              </td>
            <?php endforeach; ?>
          </tr>
        <?php endif; ?>
      <?php endforeach; ?>
    </tbody>
  </table>
</body>
</html>
