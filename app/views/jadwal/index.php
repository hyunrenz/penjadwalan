<!-- JADWAL -->
<!--  <//?php $jabatan = $this->session->userdata('user')['jabatan']; if($jabatan != 'Waka Kurikulum'){?>style="display: none;" <//?php }?> -->

<section class="content-header">
	<h1>
		Dashboard
		<small>Control panel</small>
	</h1>
	<ol class="breadcrumb">
		<li><a href="<?= site_url('') ?>"><i class="fa fa-dashboard"></i> Beranda</a></li>
		<li class="active">Jadwal</li>
	</ol>
</section>

<section class="content" >
	<div class="row">
		<div class="col-sm-12">
			<div class="box box-default">
				<div class="box-header">
					<h3 class="box-title">Data Jadwal</h3>
				</div>
				<div class="box-header">
					<a href="<?= site_url('jadwal/add') ?>" class="btn btn-success">
						<i class="fa fa-plus"></i> Tambah Jadwal
					</a>
					<button class="btn btn-primary" id="print">
						<i class="fa fa-print"></i>
					</button>
				</div>
				<div class="box-body">
					<table class="table table-bordered table-striped" id="tabeljadwal" style="width:100%">
						<thead>
							<tr>
								<th>Tahun</th>
								<th>Hari</th>
								<th>Kelas</th>
								<th>Pengajar</th>
								<th>Mata Pelajaran</th>
								<th>Jam Mulai</th>
								<th>Jam Selesai</th>
								<th width="20%">Aksi</th>
							</tr>
						</thead>
						<tbody>
							<?php foreach ($jadwal as $k): ?>
								<tr>
									<td><?= $k['tahun_ajaran'] ?></td>
									<td><?= $k['hari'] ?></td>
									<td><?= $k['kelas'] ?></td>
									<td><?= !empty($k['nama'])?$k['nama']:"-" ?></td>
									<td><?= !empty($k['mapel'])?$k['mapel']:"-" ?></td>
									<td><?= $k['jam_mulai'] ?></td>
									<td><?= $k['jam_selesai'] ?></td>
									<td>
										<a href="<?= site_url('jadwal/edit/' . $k['id_jadwal']) ?>" class="btn btn-primary">
											<i class="fa fa-edit"></i>
										</a>
										<button onclick="hapus('<?= $k['id_jadwal'] ?>')" class="btn btn-danger">
											<i class="fa fa-trash"></i>
										</button>
									</td>
								</tr>
							<?php endforeach; ?>
						</tbody>
					</table>
				</div>
			</div>
		</div>
	</div>
</section>

<div class="modal fade" id="modal-danger">
	<div class="modal-dialog modal-sm">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal" aria-label="Close">
					<span aria-hidden="true">&times;</span>
				</button>
				<h4 class="modal-title">Konfirmasi Box</h4>
			</div>
			<div class="modal-body">
				<p>Apakah anda yakin ingin menghapus&hellip;</p>
			</div>
			<div class="modal-footer">
				<button type="button" class="btn btn-default" data-dismiss="modal">Tidak</button>
				<a id="hapus" href="" class="btn btn-danger">Ya</a>
			</div>
		</div>
	</div>
</div>

<div class="modal fade" id="cetakjadwal">
	<div class="modal-dialog modal-sm modal-primary">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal" aria-label="Close">
					<span aria-hidden="true">&times;</span>
				</button>
				<h4 class="modal-title">Cetak Jadwal</h4>
			</div>
			<?= form_open('Jadwal/cetak_jadwal_kelas/'.uuid(),['id'=>'form_jadwal','target'=>'_blank']) ?>
			<div class="modal-body">
				<div class="form-group">
					<label for="pilihan_jadwal">Pilihan Jadwal</label>
					<select class="form-control" id="pilihan_jadwal">
						<option value="kelas">Jadwal Kelas</option>
						<option value="guru">Jadwal Guru</option>
					</select>
				</div>
				<div class="form-group" id="form_group_kelas">
					<label for="id_kelas">Kelas</label>
					<select id="id_kelas" class="form-control" name="id_kelas">
						<?php foreach ($kelas as $k): ?>
							<option value="<?= $k['id_kelas'] ?>"><?= $k['tahun'] ?> - <?= $k['kelas'] ?></option>
						<?php endforeach; ?>
					</select>
				</div>
				<div class="form-group" style="display:none" id="form_group_tahun">
					<label for="tahun">Tahun</label>
					<select id="tahun" class="form-control" name="tahun" disabled>
						<?php foreach ($tahun as $t): ?>
							<option value="<?= $t['tahun'] ?>"><?= $t['tahun'] ?></option>
						<?php endforeach; ?>
					</select>
				</div>
				<div class="form-group" style="display:none" id="form_group_nip">
					<label for="nip">Pengajar</label>
					<select id="nip" class="form-control" name="nip" disabled>
						<?php foreach ($guru as $g): ?>
							<option value="<?= $g['nip'] ?>"><?= $g['nama'] ?></option>
						<?php endforeach; ?>
					</select>
				</div>
			</div>
			<div class="modal-footer">
				<button id="btn-submit" type="button" class="btn btn-default" data-dismiss="modal">Batal</button>
				<input type="submit" id="hapus" class="btn btn-primary" value="Cetak">
			</div>
			<?= form_close() ?>
		</div>
	</div>
</div>



<script type="text/javascript">
$(function () {
	$('#tabeljadwal').DataTable()
	$('#print').on('click',function(){
		$('#cetakjadwal').modal();
	});
	$('#pilihan_jadwal').on('change',function(){
		if($(this).val() == "kelas"){
			$("#form_jadwal").attr('action',"<?= site_url('Jadwal/cetak_jadwal_kelas/'.uuid()) ?>");
			$("#nip,#tahun").attr('disabled',true);
			$("#id_kelas").removeAttr('disabled');
			$("#form_group_kelas").css('display','block');
			$("#form_group_tahun,#form_group_nip").css('display','none');
		}else{
			$("#form_jadwal").attr('action',"<?= site_url('Jadwal/cetak_jadwal_guru/'.uuid()) ?>");
			$("#nip,#tahun").removeAttr('disabled');
			$("#id_kelas").attr('disabled',true);
			$("#form_group_kelas").css('display','none');
			$("#form_group_tahun,#form_group_nip").css('display','block');
		}
	});
	$('#form_jadwal').on('submit',function(){
		location.reload();
	});
})
function hapus(id){
	$(function () {
		$('#hapus').attr('href','<?= site_url('jadwal/update_status/') ?>'+id)
		$('#modal-danger').modal();
	})
}
</script>
