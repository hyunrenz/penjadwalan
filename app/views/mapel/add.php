<?php if($this->session->has_userdata('mapelada')){ ?>
<script type="text/javascript">
  alert('Mata Pelajaran sudah ada...')
</script>
<?php } ?>

<section class="content-header">
  <h1>
    Dashboard
    <small>Control panel</small>
  </h1>
  <ol class="breadcrumb">
    <li><a href="<?= site_url('') ?>"><i class="fa fa-dashboard"></i> Beranda</a></li>
    <li><a href="<?= site_url('mapel') ?>">Mata Pelajaran</a></li>
    <li class="active">Tambah</li>
  </ol>
</section>

<section class="content">
  <div class="row">
    <div class="col-sm-12">
      <div class="box box-primary">
        <div class="box-header">
          <h3 class="box-title">Tambah Mapel</h3>
        </div>
        <div class="box-header">
          <a class="btn btn-success" href="<?= site_url('jenis') ?>">
            <i class="fa fa-plus"></i> Tambah Jenis Mata Pelajaran
          </a>
        </div>
        <?= form_open_multipart('mapel/store') ?>
        <div class="box-body">
          <div class="form-group">
            <label for="nama_mapel">Nama Mata Pelajaran</label>
            <input autocomplete="off" type="text" id="nama_mapel" name="nama_mapel" value="<?= set_value('nama_mapel') ?>" class="form-control" placeholder="contoh : Bahasa Indonesia">
            <?= form_error('nama_mapel', '<p class="alert alert-danger">', '</p>'); ?>
          </div>
          <div class="form-group">
            <label for="id_jenismapel">Jenis Mata Pelajaran</label>
            <select class="form-control" name="id_jenismapel" id="id_jenismapel">
              <?php foreach ($jenis_mapel as $j): ?>
                <option <?= $j['id_jenismapel'] == set_value('id_jenismapel') ?> value="<?= $j['id_jenismapel'] ?>"><?= $j['jenis_mapel'] ?></option>
              <?php endforeach; ?>
            </select>
            <?= form_error('id_jenismapel', '<p class="alert alert-danger">', '</p>'); ?>
          </div>
        </div>
        <div class="box-footer">
          <button type="submit" class="btn btn-primary">Submit</button>
        </div>
        <?= form_close() ?>
      </div>
    </div>
  </div>
</section>
<script type="text/javascript">
// $(function(){
//   $('#tahun').datepicker({
//     autoclose: true,
//     format: "yyyy",
//     viewMode: "years",
//     minViewMode: "years"
//   });
// });
</script>
