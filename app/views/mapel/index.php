
<section class="content-header">
  <h1>
    Dashboard
    <small>Control panel</small>
  </h1>
  <ol class="breadcrumb">
    <li><a href="<?= site_url('') ?>"><i class="fa fa-dashboard"></i> Beranda</a></li>
    <li class="active">Mata Pelajaran</li>
  </ol>
</section>

<section class="content">
  <div class="row">
    <div class="col-sm-12">
      <div class="box box-default">
        <div class="box-header">
          <h3 class="box-title">Data Mata Pelajaran</h3>
        </div>
        <div class="box-header">
          <a href="<?= site_url('mapel/add') ?>" class="btn btn-success">
            <i class="fa fa-plus"></i> Tambah Mata Pelajaran
          </a>

        </div>
        <div class="box-body">
          <table class="table table-bordered table-striped" id="tabelmapel" style="width:100%">
            <thead>
              <tr>
                <th>Nama</th>
                <th>Jenis</th>
                <th>Aksi</th>
              </tr>
            </thead>
            <tbody>
              <?php foreach ($mapel as $k): ?>
                <tr>
                  <td><?= $k['nama_mapel'] ?></td>
                  <td><?= $k['jenis_mapel'] ?></td>
                  <td>
                    <a href="<?= site_url('mapel/edit/'.$k['id_mapel']) ?>" class="btn btn-primary">
                      <i class="fa fa-edit"></i>
                    </a>
                    <button onclick="hapus('<?= $k['id_mapel'] ?>')" class="btn btn-danger">
                      <i class="fa fa-trash"></i>
                    </button>
                  </td>
                </tr>
              <?php endforeach; ?>
            </tbody>
          </table>
        </div>
      </div>
    </div>
  </div>
</section>
<div class="modal fade" id="modal-danger">
  <div class="modal-dialog modal-sm">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
        <h4 class="modal-title">Konfirmasi Box</h4>
      </div>
      <div class="modal-body">
        <p>Apakah anda yakin ingin menghapus&hellip;</p>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Tidak</button>
        <a id="hapus" href="" class="btn btn-danger">Ya</a>
      </div>
    </div>
  </div>
</div>

<script type="text/javascript">
$(function () {
  $('#tabelmapel').DataTable()
})
function hapus(id){
  $(function () {
    $('#hapus').attr('href','<?= site_url('mapel/update_status/') ?>'+id)
    $('#modal-danger').modal();
  })
}
</script>
