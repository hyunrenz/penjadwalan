<section class="content-header">
  <h1>
    Dashboard
    <small>Control panel</small>
  </h1>
  <ol class="breadcrumb">
    <li><a href="<?= site_url('') ?>"><i class="fa fa-dashboard"></i> Home</a></li>
    <li><a href="<?= site_url('pengumuman') ?>"></i> Pengumuman</a></li>
    <li class="active">Tambah</li>
  </ol>
</section>

<section class="content">
  <div class="row">
    <div class="col-sm-12">
      <div class="box box-solid">
        <div class="box-header with-border" style="border-color:#DDD">
          <div class="box-title">
            Tambah Pengumuman
          </div>
        </div>
        <?= form_open('pengumuman/store') ?>
        <div class="box-body">
          <div class="form-group">
            <label for="judul">Judul</label>
            <input id="judul" class="form-control" type="text" placeholder="Judul" name="judul" value="<?= set_value('judul') ?>">
            <?= form_error('judul','<p class="alert alert-danger">','</p>') ?>
          </div>
          <div class="form-group">
            <label for="pengumuman">Pengumuman</label>
            <textarea id="pengumuman" name="pengumuman" class="textarea" placeholder="Pengumuman" style="width: 100%; height: 200px; font-size: 14px; line-height: 18px; border: 1px solid #dddddd; padding: 10px;"><?= set_value('pengumuman') ?></textarea>
            <?= form_error('pengumuman','<p class="alert alert-danger">','</p>') ?>
          </div>
        </div>
        <div class="box-footer">
          <button type="submit" class="btn btn-primary">Submit</button>
        </div>
        <?php form_close() ?>
      </div>
    </div>
  </div>
</section>
