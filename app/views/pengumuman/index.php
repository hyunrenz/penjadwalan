<section class="content-header">
  <h1>
    Dashboard
    <small>Control panel</small>
  </h1>
  <ol class="breadcrumb">
    <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
    <li class="active">Pengumuman</li>
  </ol>
</section>

<section class="content">
  <div class="row">
    <div class="col-sm-12"  <?php $jabatan = $this->session->userdata('user')['jabatan']; if($jabatan != 'Waka Kurikulum'){?>style="display: none;" <?php }?>>
      <a href="<?= site_url('pengumuman/add') ?>" class="btn btn-primary">
        <i class="fa fa-bullhorn"></i> Tambah Pengumuman
      </a>
    </div>
  </div>
  <br>
  <?php foreach ($pengumuman as $p): ?>
    <div class="row">
      <div class="col-sm-12">
        <div class="box box-solid">
          <div class="box-header with-border" style="border-color:#DDD">
            <div class="box-title">
              <?= $p['judul'] ?>
            </div>
            <br>
            <small><?= $p['created_at'] ?></small>
            <div class="box-tools pull-right" <?php if($jabatan != 'Waka Kurikulum'){?>style="display: none;" <?php }?>>
              <a href="<?= site_url('pengumuman/edit/'.$p['id_pengumuman']) ?>" class="btn btn-warning btn-sm">
                <i class="fa fa-pencil"></i>
              </a>
              <button onclick="hapus('<?= $p['id_pengumuman'] ?>')" class="btn btn-sm btn-danger">
                <i class="fa fa-trash"></i>
              </button>
            </div>
          </div>
          <div class="box-body">
            <?= $p['pengumuman'] ?>
          </div>
        </div>
      </div>
    </div>
  <?php endforeach; ?>
</section>
<div class="modal fade" id="modal-danger">
  <div class="modal-dialog modal-sm">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
        <h4 class="modal-title">Konfirmasi Box</h4>
      </div>
      <div class="modal-body">
        <p>Apakah anda yakin ingin menghapus&hellip;</p>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Tidak</button>
        <a id="hapus" href="" class="btn btn-danger">Ya</a>
      </div>
    </div>
  </div>
</div>

<script type="text/javascript">
function hapus(id){
  $(function () {
    $('#hapus').attr('href','<?= site_url('pengumuman/delete/') ?>'+id)
    $('#modal-danger').modal();
  })
}
</script>
