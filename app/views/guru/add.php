<section class="content-header">
	<h1>
		Dashboard
		<small>Control panel</small>
	</h1>
	<ol class="breadcrumb">
		<li><a href="<?= site_url('') ?>"><i class="fa fa-dashboard"></i> Beranda</a></li>
		<li><a href="<?= site_url('guru') ?>">Guru</a></li>
		<li class="active">Tambah</li>
	</ol>
</section>

<section class="content">
	<div class="row">
		<div class="col-sm-12">
			<div class="box box-primary">
				<div class="box-header">
					<h3 class="box-title">Tambah Guru</h3>
				</div>
				<?= form_open_multipart('guru/store') ?>
				<div class="box-body">
					<div class="col-md-12">
						<div class="form-group">
							<label for="">Pilih Foto</label>
							<input type="file" name="foto" value="" required>
							<?php
							if (!empty($this->session->flashdata('foto'))) {
								echo $this->session->flashdata('foto');
							}
							?>
						</div>
						<div class="form-group">
							<label for="nip">Nomor Induk Pokok(NIP)</label>
							<input type="text" id="nip" name="nip" value="<?= set_value('nip') ?>" class="form-control"
								   placeholder="contoh : 1247xxxxx">
							<?= form_error('nip', '<p class="alert alert-danger">', '</p>') ?>
						</div>
						<div class="form-group">
							<label for="nama">Nama</label>
							<input type="text" id="nama" name="nama" value="<?= set_value('nama') ?>"
								   class="form-control" placeholder="contoh : Budi Wen">
							<?= form_error('nama', '<p class="alert alert-danger">', '</p>') ?>
						</div>
						<div class="form-group">
							<label for="username">Username</label>
							<input type="text" id="username" name="username" value="<?= set_value('username') ?>"
								   class="form-control" placeholder="contoh : budiwen">
							<?= form_error('username', '<p class="alert alert-danger">', '</p>') ?>
						</div>
						<div class="form-group">
							<label for="password">Password</label>
							<input type="password" id="password" name="password" value="<?= set_value('password') ?>"
								   class="form-control" placeholder="">
							<?= form_error('password', '<p class="alert alert-danger">', '</p>') ?>
						</div>
						<div class="form-group">
							<label for="jenis_kelamin">Jenis Kelamin</label>
							<select class="form-control" name="jenis_kelamin" id="jenis_kelamin">
								<option value="L" <?= set_value('jenis_kelamin') == "L" ? "selected" : "" ?>>Laki-laki
								</option>
								<option value="P" <?= set_value('jenis_kelamin') == "P" ? "selected" : "" ?>>Perempuan
								</option>
							</select>
							<?= form_error('jenis_kelamin', '<p class="alert alert-danger">', '</p>') ?>
						</div>
						<div class="form-group">
							<label for="tanggal_lahir">Tanggal Lahir</label>
							<div class="input-group date">
								<div class="input-group-addon">
									<i class="fa fa-calendar"></i>
								</div>
								<input type="text" value="<?= set_value('tanggal_lahir') ?>" id="tanggal_lahir"
									   name="tanggal_lahir" class="form-control" autocomplete="off">
							</div>
							<?= form_error('tanggal_lahir', '<p class="alert alert-danger">', '</p>') ?>
						</div>
						<div class="form-group">
							<label for="email">Email</label>
							<div class="input-group">
								<div class="input-group-addon">
									<i class="fa fa-envelope"></i>
								</div>
								<input type="email" value="<?= set_value('email') ?>" id="email" name="email"
									   class="form-control" placeholder="contoh : BudiWen@yahoo.com">
							</div>
							<?= form_error('email', '<p class="alert alert-danger">', '</p>') ?>
						</div>
						<div class="form-group">
							<label for="jabatan">Jabatan</label>
							<select class="form-control" name="jabatan" id="jabatan">
								<option value="Tetap" <?= set_value('jabatan') == "Tetap" ? "selected" : "" ?>>Tetap</option>
								<option value="Tidak Tetap" <?= set_value('jabatan') == "Tidak Tetap" ? "selected" : "" ?>>Tidak Tetap</option>
								<option value="Waka Kurikulum" <?= set_value('jabatan') == "Waka Kurikulum" ? "selected" : "" ?>>Waka Kurikulum</option>
								<option value="Waka Kesiswaan" <?= set_value('jabatan') == "Waka Kesiswaan" ? "selected" : "" ?>>Waka Kesiswaan</option>
								<option value="Kepala Sekolah" <?= set_value('jabatan') == "Kepala Sekolah" ? "selected" : "" ?>>Kepala Sekolah</option>
							</select>
							<?= form_error('jabatan', '<p class="alert alert-danger">', '</p>') ?>
						</div>
						<div class="form-group">
							<label for="tanggal_masuk">Tanggal Pertama Kerja</label>
							<div class="input-group date">
								<div class="input-group-addon">
									<i class="fa fa-calendar"></i>
								</div>
								<input type="text" autocomplete="off" value="<?= set_value('tanggal_masuk') ?>"
									   id="tanggal_masuk" name="tanggal_masuk" class="form-control">
							</div>
							<?= form_error('tanggal_masuk', '<p class="alert alert-danger">', '</p>') ?>
						</div>
						<div class="form-group">
							<label for="telepon">Nomor Telepon</label>
							<div class="input-group">
								<div class="input-group-addon">
									<i class="fa fa-phone"></i>
								</div>
								<input type="tel" value="<?= set_value('telepon') ?>" id="telepon" name="telepon"
									   class="form-control">
							</div>
							<?= form_error('telepon', '<p class="alert alert-danger">', '</p>') ?>
						</div>
						<div class="form-group">
							<label for="alamat">Alamat</label>
							<textarea id="alamat" name="alamat" class="form-control" rows="3"
									  placeholder="Contoh : Jl. Rajawali No.xx"><?= set_value('alamat') ?></textarea>
							<?= form_error('alamat', '<p class="alert alert-danger">', '</p>') ?>
						</div>
						<!-- <div class="form-group">
						<label for="ijazah">Pilih File Ijazah</label>
						<input type="file" id="ijazah" name="ijazah">
						<small>*Ektensi File berbentuk file.pdf</small>
					  </div> -->
						<div class="form-group">
							<input type="checkbox" name="agree" id="agree">
							<label for="agree">Saya menyatakan berkas yang dimasukkan ialah benar adanya</label>
							<?= form_error('agree', '<p class="alert alert-danger">', '</p>') ?>
						</div>
					</div>
				</div>
				<div class="box-footer">
					<div class="col-sm-12" style="margin-bottom:10px;">
						<button type="submit" class="btn btn-primary">Daftar</button>
					</div>
				</div>
				<?= form_close() ?>
			</div>
		</div>
	</div>
</section>

<script type="text/javascript">
    $(function () {
        $('#tabelguru').DataTable();
        $('#tanggal_lahir').datepicker({
            autoclose: true,
            format: 'yyyy-mm-dd'
        });
        $('#tanggal_masuk').datepicker({
            autoclose: true,
            format: 'yyyy-mm-dd'
        });
    })
</script>
