
<section class="content-header">
  <h1>
    Dashboard
    <small>Control panel</small>
  </h1>
  <ol class="breadcrumb">
    <li><a href="<?= site_url('') ?>"><i class="fa fa-dashboard"></i> Beranda</a></li>
    <li><a href="<?= site_url('guru') ?>">Guru</a></li>
    <li class="active">Detail</li>
  </ol>
</section>

<section class="content">
  <div class="row">
    <div class="col-sm-8">
      <div class="box box-primary">
        <div class="box-header">
          <h3 class="box-title">Detail Guru</h3>
          <a class="btn btn-primary" href="<?= site_url('guru/edit/'.$guru['nip']) ?>">
            <i class="fa fa-pencil"></i>
          </a>
        </div>
        <div class="box-body">
          <table class="table" style="width:100%">
            <tbody>
              <tr>
                <td>NIP</td>
                <td><?= $guru['nip'] ?></td>
              </tr>
              <tr>
                <td>Nama</td>
                <td><?= $guru['nama'] ?></td>
              </tr>
              <tr>
                <td>Jenis Kelamin</td>
                <td><?= $guru['jenis_kelamin'] == "L" ? "Laki-laki":"Perempuan" ?></td>
              </tr>
              <tr>
                <td>Tanggal Lahir</td>
                <td><?= $guru['tanggal_lahir'] ?></td>
              </tr>
              <tr>
                <td>Email</td>
                <td><?= $guru['email'] ?></td>
              </tr>
              <tr>
                <td>Alamat</td>
                <td><?= $guru['alamat'] ?></td>
              </tr>
              <tr>
                <td>Jabatan</td>
                <td><?= $guru['jabatan'] ?></td>
              </tr>
              <tr>
                <td>Tanggal Pertama Kerja</td>
                <td><?= $guru['tanggal_masuk'] ?></td>
              </tr>
              <tr>
                <td>Status</td>
                <td><?= $guru['status'] == "1"?"Aktif":"Tidak Aktif" ?></td>
              </tr>
              <tr>
                <?php if($guru['status'] == '1'){ ?>
                  <td>Terakhir di Non-Aktif</td>
                <?php }else{ ?>
                  <td>Tanggal Non-Aktif</td>
                <?php } ?>
                <td><?= !empty($guru['tanggal_nonaktif'])?date('Y-m-d',strtotime($guru['tanggal_nonaktif'])):"-" ?></td>
              </tr>
              <tr>
                <td>Nomor Telpon</td>
                <td><?= $guru['telepon'] ?></td>
              </tr>
            </tbody>
          </table>
        </div>
      </div>
    </div>

    <div class="col-sm-4">
      <div class="box box-primary">
        <div class="box-body">
          <img class="img-thumbnail" src="<?= base_url('assets/foto/'.$guru['foto']) ?>" alt="">
        </div>
      </div>
    </div>
  </div>
</section>
