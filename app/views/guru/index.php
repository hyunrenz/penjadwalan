
<section class="content-header">
  <h1>
    Dashboard
    <small>Control panel</small>
  </h1>
  <ol class="breadcrumb">
    <li><a href="<?= site_url('') ?>"><i class="fa fa-dashboard"></i> Beranda</a></li>
    <li class="active">Guru</li>
  </ol>
</section>

<section class="content">
  <div class="row">
    <div class="col-sm-12">
      <div class="box box-default">
        <div class="box-header">
          <h3 class="box-title">Data Guru</h3>
        </div>
        <div class="box-header">
          <a href="<?= site_url('guru/add') ?>" class="btn btn-success">
            <i class="fa fa-plus"></i> Tambah Guru
          </a>

        </div>
        <div class="box-body">
          <table class="table table-bordered table-striped" id="tabelguru" style="width:100%">
            <thead>
              <tr>
                <th>NIP</th>
                <th>Nama</th>
                <th>Username</th>
                <th>Alamat</th>
                <th>Jabatan</th>
                <th>No Telepon</th>
                <th>Tanggal Lahir</th>
                <th>Status</th>
                <th>Aksi</th>
              </tr>
            </thead>
            <tbody>
              <?php foreach ($guru as $g): ?>
                <tr>
                  <td><?= $g['nip'] ?></td>
                  <td><?= $g['nama'] ?></td>
                  <td><?= $g['username'] ?></td>
                  <td><?= $g['alamat'] ?></td>
                  <td><?= $g['jabatan'] ?></td>
                  <td><?= $g['telepon'] ?></td>
                  <td><?= $g['tanggal_lahir'] ?></td>
                  <td><?= $g['status'] == "1"?"Aktif":"Tidak Aktif" ?></td>
                  <td>
                    <a href="<?= site_url('guru/detail/'.$g['nip']) ?>" class="btn btn-success">
                      <i class="fa fa-search"></i>
                    </a>
                    <a href="<?= site_url('guru/edit/'.$g['nip']) ?>" class="btn btn-primary">
                      <i class="fa fa-edit"></i>
                    </a>
                    <!-- <a href="<?= site_url('guru/update_status/'.$g['nip']) ?>" class="btn btn-warning">
                      <i class="fa fa-eye"></i>
                    </a> -->
                    <button onclick="update_status('<?= $g['nip'] ?>')" class="btn btn-warning">
                      <i class="fa fa-eye"></i>
                    </button>
                  </td>
                </tr>
              <?php endforeach; ?>
            </tbody>
          </table>
        </div>
      </div>
    </div>
  </div>
</section>
<div class="modal fade" id="modal-danger">
  <div class="modal-dialog modal-sm">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
        <h4 class="modal-title">Konfirmasi Box</h4>
      </div>
      <div class="modal-body">
        <p>Apakah anda yakin ingin mengubah status&hellip;</p>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Tidak</button>
        <a id="update_status" href="" class="btn btn-warning">Ya</a>
      </div>
    </div>
  </div>
</div>

<script type="text/javascript">
$(function () {
  $('#tabelguru').DataTable()
})
function update_status(id){
  $(function () {
    $('#update_status').attr('href','<?= site_url('guru/update_status/') ?>'+id)
    $('#modal-danger').modal();
  })
}
</script>
