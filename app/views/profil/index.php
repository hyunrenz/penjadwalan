
<section class="content-header">
	<h1>
		Dashboard
		<small>Control panel</small>
	</h1>
	<ol class="breadcrumb">
		<li><a href="<?= site_url('') ?>"><i class="fa fa-dashboard"></i> Beranda</a></li>
		<li><a href="<?= site_url('profil') ?>">profil</a></li>
		<li class="active">Detail</li>
	</ol>
</section>

<section class="content">
	<div class="row">
		<div class="col-sm-8">
			<div class="box box-primary">
				<div class="box-header">
					<h3 class="box-title">Detail Saya</h3>
					<a class="btn btn-primary" href="<?= site_url('profil/edit/'.$this->session->userdata('user')['nip']) ?>">
						<i class="fa fa-pencil"></i>
					</a>
				</div>
				<div class="box-body">
					<table class="table" style="width:100%">
						<tbody>
						<tr>
							<td>NIP</td>
							<td><?= $this->session->userdata('user')['nip']; ?></td>
						</tr>
						<tr>
							<td>Nama</td>
							<td><?=$this->session->userdata('user')['nama']; ?></td>
						</tr>
						<tr>
							<td>Jenis Kelamin</td>
							<td><?= $this->session->userdata('user')['jenis_kelamin'] == "L" ? "Laki-laki":"Perempuan" ?></td>
						</tr>
						<tr>
							<td>Tanggal Lahir</td>
							<td><?= $this->session->userdata('user')['tanggal_lahir']; ?></td>
						</tr>
						<tr>
							<td>Email</td>
							<td><?= $this->session->userdata('user')['email']; ?></td>
						</tr>
						<tr>
							<td>Alamat</td>
							<td><?= $this->session->userdata('user')['alamat']; ?></td>
						</tr>
						<tr>
							<td>Jabatan</td>
							<td><?= $this->session->userdata('user')['jabatan']; ?></td>
						</tr>
						<tr>
							<td>Tanggal Pertama Kerja</td>
							<td><?= $this->session->userdata('user')['tanggal_masuk']; ?></td>
						</tr>
						<tr>
							<!--td>Lama Kerja</td>
							<td>Lama Kerja</td-->
						</tr>
						<tr>
							<td>Status</td>
							<td><?=$this->session->userdata('user')['status']== "1"?"Aktif":"Tidak Aktif" ?></td>
						</tr>
						<tr>
							<td>Nomor Telpon</td>
							<td><?=$this->session->userdata('user')['telepon']; ?></td>
						</tr>
						<!-- <tr>
						  <td>Ijazah</td>
						  <td>
							<a href="#" class="btn btn-success" target="_self">Cek Ijazah</a>
						  </td>
						</tr> -->
						</tbody>
					</table>
				</div>
			</div>
		</div>

		<div class="col-sm-4">
			<div class="box box-primary">
				<div class="box-body">
					<img class="img-thumbnail" src="<?= base_url('assets/foto/'.$this->session->userdata('user')['foto']) ?>" alt="">
				</div>
			</div>
		</div>
	</div>
</section>
