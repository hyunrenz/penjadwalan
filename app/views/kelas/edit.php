<?php if($this->session->has_userdata('kelasada')){ ?>
  <script type="text/javascript">
  alert('Kelas untuk tahun tersebut sudah ada...')
  </script>
<?php } ?>

<section class="content-header">
  <h1>
    Dashboard
    <small>Control panel</small>
  </h1>
  <ol class="breadcrumb">
    <li><a href="<?= site_url('') ?>"><i class="fa fa-dashboard"></i> Beranda</a></li>
    <li><a href="<?= site_url('kelas') ?>">Kelas</a></li>
    <li class="active">Edit</li>
  </ol>
</section>

<section class="content">
  <div class="row">
    <div class="col-sm-12">
      <div class="box box-primary">
        <div class="box-header">
          <h3 class="box-title">Edit Kelas</h3>
        </div>
        <?= form_open_multipart('kelas/update/'.$kelas['id_kelas']) ?>
        <div class="box-body">
          <div class="form-group">
            <label for="tahun">Tahun</label>
            <input autocomplete="off" type="text" id="tahun" name="tahun" value="<?= set_value('tahun')? set_value('tahun') :$kelas['tahun'] ?>" class="form-control" placeholder="contoh : 2019">
            <?= form_error('tahun', '<p class="alert alert-danger">', '</p>'); ?>
          </div>
          <div class="form-group">
            <label for="kelas">Ruang Kelas</label>
            <input type="text" id="kelas" name="kelas" value="<?= set_value('kelas')? set_value('kelas') :$kelas['kelas'] ?>" class="form-control" placeholder="contoh : 2A">
            <?= form_error('kelas', '<p class="alert alert-danger">', '</p>'); ?>
          </div>
          <div class="form-group">
            <label for="jenis_kelas">Jenis Kelas</label>
            <select class="form-control" name="jenis_kelas">
              <?php if(set_value('jenis_kelas')): ?>
                <option <?= set_value('jenis_kelas') == "Kecil"?"selected":"" ?> value="Kecil">Kecil</option>
                <option <?= set_value('jenis_kelas') == "Besar"?"selected":"" ?> value="Besar">Besar</option>
              <?php else: ?>
                <option <?= $kelas['jenis_kelas'] == "Kecil"?"selected":"" ?> value="Kecil">Kecil</option>
                <option <?= $kelas['jenis_kelas'] == "Besar"?"selected":"" ?> value="Besar">Besar</option>
              <?php endif; ?>
            </select>
            <?= form_error('jenis_kelas', '<p class="alert alert-danger">', '</p>'); ?>
          </div>
          <div class="form-group">
            <label for="nip">Wali Kelas</label>
            <select class="form-control" id="nip" name="nip">
              <option value=""></option>
              <?php foreach ($guru as $g): ?>
                <?php if (set_value('nip')): ?>
                  <option <?= set_value('nip') == $g['nip']?"selected":"" ?> value="<?= $g['nip'] ?>"><?= $g['nama'] ?></option>
                <?php else: ?>
                  <option <?= $kelas['nip'] == $g['nip']?"selected":"" ?> value="<?= $g['nip'] ?>"><?= $g['nama'] ?></option>
                <?php endif; ?>
              <?php endforeach; ?>
            </select>
            <?= form_error('kelas', '<p class="alert alert-danger">', '</p>'); ?>
          </div>
          <div class="form-group">
            <label for="lantai">Lantai</label>
            <input type="text" id="lantai" name="lantai" value="<?= set_value('lantai')? set_value('lantai') :$kelas['lantai'] ?>" class="form-control" placeholder="contoh : 2">
            <?= form_error('lantai', '<p class="alert alert-danger">', '</p>'); ?>
          </div>
          <div class="form-group">
            <label for="deskripsi">Keterangan</label>
            <textarea id="deskripsi" name="deskripsi" class="form-control" rows="3" cols="80" placeholder="Contoh: Lantai disebelah perpustakaan"><?= set_value('deskripsi')? set_value('deskripsi') :$kelas['deskripsi'] ?></textarea>
            <?= form_error('deskripsi', '<p class="alert alert-danger">', '</p>'); ?>
          </div>
        </div>
        <div class="box-footer">
          <button type="submit" class="btn btn-primary">Submit</button>
        </div>
        <?= form_close() ?>
      </div>
    </div>
  </div>
</section>
<script type="text/javascript">
$(function(){
  $('#tahun').datepicker({
    autoclose: true,
    format: "yyyy",
    viewMode: "years",
    minViewMode: "years"
  });
});
</script>
