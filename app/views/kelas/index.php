
<section class="content-header">
  <h1>
    Dashboard
    <small>Control panel</small>
  </h1>
  <ol class="breadcrumb">
    <li><a href="<?= site_url('') ?>"><i class="fa fa-dashboard"></i> Beranda</a></li>
    <li class="active">Kelas</li>
  </ol>
</section>

<section class="content">
  <div class="row">
    <div class="col-sm-12">
      <div class="box box-default">
        <div class="box-header">
          <h3 class="box-title">Data Kelas</h3>
        </div>
        <div class="box-header">
          <a href="<?= site_url('kelas/add') ?>" class="btn btn-success">
            <i class="fa fa-plus"></i> Tambah Kelas
          </a>

        </div>
        <div class="box-body">
          <table class="table table-bordered table-striped" id="tabelkelas" style="width:100%">
            <thead>
              <tr>
                <th>Tahun</th>
                <th>Ruang Kelas</th>
                <th>Wali Kelas</th>
                <th>Jenis Kelas</th>
                <th>Lantai</th>
                <th width="20%">Keterangan</th>
                <th width="20%">Aksi</th>
              </tr>
            </thead>
            <tbody>
              <?php foreach ($kelas as $k): ?>
                <tr>
                  <td><?= $k['tahun'] ?></td>
                  <td><?= $k['kelas'] ?></td>
                  <td><?= !empty($k['nama'])?$k['nama']:"-" ?></td>
                  <td><?= $k['jenis_kelas'] ?></td>
                  <td><?= $k['lantai'] ?></td>
                  <td><?= $k['deskripsi'] ?></td>
                  <td>
                    <a href="<?= site_url('kelas/edit/'.$k['id_kelas']) ?>" class="btn btn-primary">
                      <i class="fa fa-edit"></i>
                    </a>
                    <button onclick="hapus('<?= $k['id_kelas'] ?>')" class="btn btn-danger">
                      <i class="fa fa-trash"></i>
                    </button>
                  </td>
                </tr>
              <?php endforeach; ?>
            </tbody>
          </table>
        </div>
      </div>
    </div>
  </div>
</section>
<div class="modal fade" id="modal-danger">
  <div class="modal-dialog modal-sm">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
        <h4 class="modal-title">Konfirmasi Box</h4>
      </div>
      <div class="modal-body">
        <p>Apakah anda yakin ingin menghapus&hellip;</p>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Tidak</button>
        <a id="hapus" href="" class="btn btn-danger">Ya</a>
      </div>
    </div>
  </div>
</div>

<script type="text/javascript">
$(function () {
  $('#tabelkelas').DataTable()
});
function hapus(id){
  $(function () {
    $('#hapus').attr('href','<?= site_url('kelas/update_status/') ?>'+id)
    $('#modal-danger').modal();
  })
}
</script>
