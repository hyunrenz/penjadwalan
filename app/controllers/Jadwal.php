<?php
defined("BASEPATH") OR exit("No direct script access allowed");
class Jadwal extends MY_Controller{
  public function __construct(){
    parent::__construct();
    if(!$this->check_sess()){
      redirect('auth');
    }
    // $this->load->library('excel');
  }

  public function form_rules($jenis_waktu){
    $form = $this->form_validation;
    $form->set_rules('id_kelas','Kelas','required|trim');
    if($jenis_waktu == "Kelas"){
      $form->set_rules('nip','Pengajar','required|trim');
      $form->set_rules('id_mapel','Mata Pelajaran','required|trim');
    }
    $form->set_rules('hari','Hari','required|trim');
    $form->set_rules('id_waktu','Waktu','required|trim');
  }

  public function index(){
    $data['_view'] = 'jadwal/index';
    $data['kelas'] = $this->m_kelas->get(['status'=>'1'],'','',['tahun'=>"DESC"]);
    $data['guru'] = $this->m_guru->get(['status'=>'1'],'','',['nama'=>"ASC"]);
    $data['tahun'] = $this->m_jadwal->group_tahun();
    $data['jadwal'] = $this->m_jadwal->get(['status'=>'1']);
    for ($i=0; $i < count($data['jadwal']) ; $i++) {
      $data['jadwal'][$i]['kelas'] = $this->m_kelas->get_row(['id_kelas'=>$data['jadwal'][$i]['id_kelas']])['kelas'];
      if (!empty($data['jadwal'][$i]['nip'])) {
        $data['jadwal'][$i]['nama'] = $this->m_guru->get_row(['nip'=>$data['jadwal'][$i]['nip']])['nama'];
        $data['jadwal'][$i]['mapel'] = $this->m_mapel->get_row(['id_mapel'=>$data['jadwal'][$i]['id_mapel']])['nama_mapel'];
      }else{
        $data['jadwal'][$i]['nama'] = "-";
        $data['jadwal'][$i]['mapel'] = $this->m_waktu->get_row(['id_waktu'=>$data['jadwal'][$i]['id_waktu']])['kegiatan'];
      }
      $data['jadwal'][$i]['jam_mulai'] = $this->m_waktu->get_row(['id_waktu'=>$data['jadwal'][$i]['id_waktu']])['jam_mulai'];
      $data['jadwal'][$i]['jam_selesai'] = $this->m_waktu->get_row(['id_waktu'=>$data['jadwal'][$i]['id_waktu']])['jam_selesai'];
    }
    $this->load->view('layout/index',$data);
  }

  public function jadwalguru(){
    $nip = $this->session->userdata('user')['nip'];
    $data['tahun'] = $this->input->post('tahun');
    $data['jadwal'] = empty($data['tahun'])? [] : $this->m_jadwal->jadwal_guru($nip,$data['tahun']);
    $data['listtahun'] = $this->m_jadwal->group_tahun();
    $data['_view'] = 'jadwal/jadwalguru';
    $this->load->view('layout/index',$data);
  }

  public function add(){
    $data['_view'] = 'jadwal/add';
    $data['kelas'] = $this->m_kelas->get(['status'=>'1']);
    $data['guru'] = $this->m_guru->get(['status'=>'1']);
    $data['mapel'] = $this->m_mapel->get(['status'=>'1']);
    $data['waktu'] = $this->m_waktu->get(['status'=>'1']);
    for ($i=0; $i < count($data['mapel']) ; $i++) {
      $data['mapel'][$i]['jenis_mapel'] = $this->m_jenis->get_row(['id_jenismapel'=>$data['mapel'][$i]['id_jenismapel']])['jenis_mapel'];
    }
    $this->load->view('layout/index',$data);
  }

  public function store(){
    $input = $this->input;
    $jenis_waktu = $this->m_waktu->get_row(['id_waktu'=>$input->post('id_waktu')])['jenis_kegiatan'];
    $this->form_rules($jenis_waktu);
    $form = $this->form_validation;
    if($form->run()){
      $kelas = $input->post('id_kelas');
      $tahun = $this->m_kelas->get_row(['id_kelas'=>$kelas])['tahun'];
      $jadwal = $this->m_jadwal->get_row([
        'tahun_ajaran' => $tahun,
        'hari' => $input->post('hari'),
        'id_kelas' => $kelas,
        'id_waktu' => $input->post('id_waktu'),
        'status'  => '1'
      ]);
      if(empty($jadwal)){
        $params = [
          'id_kelas' => $kelas,
          'id_waktu' => $input->post('id_waktu'),
          'hari' => $input->post('hari'),
          'tahun_ajaran' => $tahun,
        ];
        if(!empty($input->post('nip'))){
          $params['nip'] = $input->post('nip');
        }
        if(!empty($input->post('id_mapel'))){
          $params['id_mapel'] = $input->post('id_mapel');
        }
        $add = $this->m_jadwal->store($params);
        $this->session->set_flashdata('addjadwal',$add);
        redirect('jadwal');
      }else{
        $this->session->set_flashdata('jadwalada',TRUE);
        redirect('jadwal/add');
      }
    }else{
      $this->add();
    }
  }

  public function edit($where){
    $data['_view'] = 'jadwal/edit';
    $data['jadwal'] = $this->m_jadwal->get_row(['id_jadwal'=>$where]);

    $tahun = $this->m_kelas->get_row(['id_kelas'=>$data['jadwal']['id_kelas']])['tahun'];
    $data['jadwal_kelas'] = $this->m_jadwal->jadwal_kelas($data['jadwal']['id_kelas'],$tahun);
    $data['jadwal']['jenis_kelas'] = $this->m_kelas->get_row(['id_kelas'=>$data['jadwal']['id_kelas']])['jenis_kelas'];
    $data['kelas'] = $this->m_kelas->get(['status'=>'1','jenis_kelas'=>$data['jadwal']['jenis_kelas']]);
    $jenis_waktu = $this->m_waktu->get_row(['id_waktu'=>$data['jadwal']['id_waktu']])['jenis_waktu'];
    $data['waktu'] = $this->m_waktu->get(['status'=>'1','jenis_waktu'=>$jenis_waktu]);
    $data['guru'] = $this->m_guru->get(['status'=>'1']);
    $data['mapel'] = $this->m_mapel->get(['status'=>'1']);
    for ($i=0; $i < count($data['mapel']) ; $i++) {
      $data['mapel'][$i]['jenis_mapel'] = $this->m_jenis->get_row(['id_jenismapel'=>$data['mapel'][$i]['id_jenismapel']])['jenis_mapel'];
    }
    $this->load->view('layout/index',$data);
  }

  public function update($where){
    $input = $this->input;
    $jenis_waktu = $this->m_waktu->get_row(['id_waktu'=>$input->post('id_waktu')])['jenis_kegiatan'];
    $this->form_rules($jenis_waktu);
    $form = $this->form_validation;
    if($form->run()){
      $kelas = $input->post('id_kelas');
      $tahun = $this->m_kelas->get_row(['id_kelas'=>$kelas])['tahun'];
      $jadwal = $this->m_jadwal->get_row(['id_jadwal'=>$where]);
      if($jadwal['id_kelas'] != $kelas || $jadwal['hari'] != $input->post('hari') || $jadwal['id_waktu'] != $input->post('id_waktu')){
        $new_jadwal = $this->m_jadwal->get_row([
          'tahun_ajaran' => $tahun,
          'hari' => $input->post('hari'),
          'id_kelas' => $kelas,
          'id_waktu' => $input->post('id_waktu'),
          'status'  => '1'
        ]);
        if(!empty($jadwal)){
          $this->session->set_flashdata('jadwalada',TRUE);
          redirect('jadwal/edit/'.$where);
        }
      }
      $params = [
        'id_kelas' => $kelas,
        'id_waktu' => $input->post('id_waktu'),
        'hari' => $input->post('hari'),
        'tahun_ajaran' => $tahun,
      ];
      if(!empty($input->post('nip'))){
        $params['nip'] = $input->post('nip');
      }
      if(!empty($input->post('id_mapel'))){
        $params['id_mapel'] = $input->post('id_mapel');
      }
      $edit = $this->m_jadwal->update(['id_jadwal'=>$where],$params);
      $this->session->set_flashdata('editjadwal',$edit);
      redirect('jadwal');
    }else{
      $this->edit($where);
    }
  }

  public function update_status($where){
    $jadwal = $this->m_jadwal->get_row(['id_jadwal'=>$where]);
    if(count($jadwal)>0){
      if($jadwal['status'] == '1'){
        $params = ['status' => '0'];
      }else{
        $params = ['status' => '1'];
      }
      $this->m_jadwal->update(['id_jadwal'=>$where],$params);
      $this->session->set_flashdata('update_status',TRUE);
    }else{
      $this->session->set_flashdata('update_status',FALSE);
    }
    redirect('jadwal');
  }

  public function req_jadwal($kelas){
    $tahun = $this->m_kelas->get_row(['id_kelas'=>$kelas,'status'=>'1'])['tahun'];
    $response['data'] = $this->m_jadwal->jadwal_kelas($kelas,$tahun);
    json_e($response);
  }

  // public function req_jadwal_guru($nip,$tahun){
  //   $response['data'] = $this->m_jadwal->jadwal_guru($nip,$tahun);
  //   json_e($response);
  // }

  public function req_waktu($jenis){
    $response['data'] = $this->m_waktu->get(['status'=>'1','jenis_waktu'=>$jenis]);
    json_e($response);
  }

  public function req_jenis_waktu($waktu){
    $response['data'] = $this->m_waktu->get_row(['status'=>'1','id_waktu'=>$waktu]);
    json_e($response);
  }

  public function req_kelas($jenis){
    $response['data'] = $this->m_kelas->get(['status'=>'1','jenis_kelas'=>$jenis]);
    json_e($response);
  }

  public function cetak_jadwal_guru($value='')
  {
    $tahun = $this->input->post('tahun');
    $data['tahun'] = $tahun;
    if(!empty($this->input->post('nip'))){
      $nip = $this->input->post('nip');//WAKA
    }else{
      $nip = $this->session->userdata('user')['nip'];//GURU
    }
    if (strlen($tahun) != 4) {
      show_404(); // JIKA TIDAK DAPAT TAHUN
    }
    $data['waktu_kecil'] = $this->m_waktu->get(
      [
        'status'=>'1',
        'jenis_waktu'=>'Kecil'
      ],
      '','',
      [
        'jam_mulai' => "ASC"
      ]
    );
    $data['waktu_besar'] = $this->m_waktu->get(
      [
        'status'=>'1',
        'jenis_waktu'=>'Besar'
      ],
      '','',
      [
        'jam_mulai' => "ASC"
      ]
    );
    $data['jadwal'] = $this->m_jadwal->get([
      'nip' => $nip,
      'status' => '1',
    ]);
    $data['nama'] = $this->session->userdata('user')['nama'];
    for ($i=0; $i < count($data['jadwal']) ; $i++) {
      $data['jadwal'][$i]['jenis_kelas'] = $this->m_kelas->get_row(['id_kelas'=>$data['jadwal'][$i]['id_kelas']])['jenis_kelas'];
      $data['jadwal'][$i]['kelas'] = $this->m_kelas->get_row(['id_kelas'=>$data['jadwal'][$i]['id_kelas']])['kelas'];
      if (!empty($data['jadwal'][$i]['nip'])) {
        $data['jadwal'][$i]['nama_mapel'] = $this->m_mapel->get_row(['id_mapel'=>$data['jadwal'][$i]['id_mapel']])['nama_mapel'];
        $data['jadwal'][$i]['nama_pengajar'] = $this->m_guru->get_row(['nip'=>$data['jadwal'][$i]['nip']])['nama'];
      }
    }

    $mpdf = new \Mpdf\Mpdf(['orientation'=>'L']);

    $mpdf->AddPage();
    $html = $this->load->view('jadwal/cetak_jadwal_guru_kecil',$data,true);
    $mpdf->WriteHTML($html);

    $mpdf->AddPage();
    $html = $this->load->view('jadwal/cetak_jadwal_guru_besar',$data,true);
    $mpdf->WriteHTML($html);

    $tanggal = date_indonesian($this->m_jadwal->datenow())." ".date('H i s');
    $mpdf->Output('Jadwal_guru_'.$tanggal.".pdf",'D');
  }

  public function cetak_jadwal_kelas($value=''){
    $kelas = $this->m_kelas->get_row(['id_kelas'=>$this->input->post('id_kelas')]);
    if(empty($kelas)){
      show_404();
    }
    $data['waktu'] = $this->m_waktu->get(
      [
        'status'=>'1',
        'jenis_waktu'=>$kelas['jenis_kelas']
      ],
      '','',
      [
        'jam_mulai' => "ASC"
      ]
    );
    $data['jadwal'] = $this->m_jadwal->get([
      'id_kelas' => $kelas['id_kelas'],
      'status' => '1',
    ]);
    $data['kelas'] = $kelas;
    for ($i=0; $i < count($data['jadwal']); $i++) {
      if (!empty($data['jadwal'][$i]['nip'])) {
        $data['jadwal'][$i]['nama_mapel'] = $this->m_mapel->get_row(['id_mapel'=>$data['jadwal'][$i]['id_mapel']])['nama_mapel'];
        $data['jadwal'][$i]['nama_pengajar'] = $this->m_guru->get_row(['nip'=>$data['jadwal'][$i]['nip']])['nama'];
      }
    }

    $html = $this->load->view('jadwal/cetak_jadwal_kelas',$data,TRUE);
    $mpdf = new \Mpdf\Mpdf(['orientation' => 'L']);
    $mpdf->WriteHTML($html);
    $tanggal = date_indonesian($this->m_mapel->datenow())." ".date('H i s');
    $mpdf->Output('Jadwal_Kelas_'.$tanggal.".pdf",'D');
  }
}
