<?php
defined("BASEPATH") OR exit("No direct script access allowed");
class Kelas extends MY_Controller{

  public function __construct(){
    parent::__construct();
    if(!$this->check_sess()){
      redirect('auth');
    }
  }

  public function form_rules(){
    $form = $this->form_validation;
    $form->set_rules('tahun','Tahun','required|trim');
    $form->set_rules('kelas','Ruang Kelas','required|trim');
    $form->set_rules('jenis_kelas','Jenis Kelas','required|trim');
    $form->set_rules('lantai','Lantai','required|trim');
    $form->set_rules('deskripsi','Keterangan','trim');
  }

  public function index(){
    $data['_view'] = 'kelas/index';
    $data['kelas'] = $this->m_kelas->get(['status' => '1']);
    for ($i=0; $i < count($data['kelas']) ; $i++) {
      $data['kelas'][$i]['nama'] = $this->m_guru->get_row(['nip'=>$data['kelas'][$i]['nip']])['nama'];
    }
    $this->load->view('layout/index',$data);
  }

  public function add(){
    $data['_view'] = 'kelas/add';
    $data['guru'] = $this->m_guru->get(['status'=>'1']);
    $this->load->view('layout/index',$data);
  }

  public function store(){
    $this->form_rules();
    $form = $this->form_validation;
    $input = $this->input;
    if($form->run()){
      $kelas = $this->m_kelas->get_row([
        'tahun'=> $input->post('tahun'),
        'kelas' => $input->post('kelas'),
        'jenis_kelas' => $input->post('jenis_kelas'),
        'status'=>'1'
      ]);
      if(empty($kelas)){
        $params = [
          'tahun' => $input->post('tahun'),
          'kelas' => $input->post('kelas'),
          'jenis_kelas' => $input->post('jenis_kelas'),
          'lantai' => $input->post('lantai'),
          'deskripsi' => $input->post('deskripsi'),
        ];
        $nip = $input->post('nip');
        if(!empty($nip)){
          $params['nip'] = $nip;
          $munas_id = $this->m_jenis->get_row(['jenis_mapel'=>'Muatan Internasional'])['id_jenismapel'];
          $mapel_munas = $this->m_mapel->get(['id_jenismapel'=>$munas_id]);
          for ($i=0; $i < count($mapel_munas); $i++) {
            $params2 = [
              'nip' => $nip,
              'id_mapel' => $mapel_munas['id_mapel']
            ];
            $this->m_pengajar->store($params2);
          }
        }
        $add = $this->m_kelas->store($params);
        $this->session->set_flashdata('addkelas',$add);
        redirect('kelas');
      }else{
        $this->session->set_flashdata('kelasada',TRUE);
        redirect('kelas/add');
      }
    }else{
      $this->add();
    }
  }

  public function edit($where){
    $data['_view'] = 'kelas/edit';
    $data['guru'] = $this->m_guru->get(['status'=>'1']);
    $data['kelas'] = $this->m_kelas->get_row(['id_kelas'=>$where]);
    $this->load->view('layout/index',$data);
  }

  public function update($where){
    $this->form_rules();
    $form = $this->form_validation;
    $input = $this->input;
    if($form->run()){
      $kelas = $this->m_kelas->get_row(['id_kelas'=>$where]);
      if($kelas['tahun'] != $input->post('tahun') || $kelas['kelas'] != $input->post('kelas') || $kelas['jenis_kelas'] != $input->post('jenis_kelas')){
        $new_kelas = $this->m_kelas->get_row([
          'tahun'=> $input->post('tahun'),
          'kelas' => $input->post('kelas'),
          'jenis_kelas' => $input->post('jenis_kelas'),
          'status'=>'1'
        ]);
        if(!empty($new_kelas)){
          $this->session->set_flashdata('kelasada',TRUE);
          redirect('kelas/edit/'.$where);
        }
      }
      $params = [
        'tahun' => $input->post('tahun'),
        'kelas' => $input->post('kelas'),
        'jenis_kelas' => $input->post('jenis_kelas'),
        'lantai' => $input->post('lantai'),
        'deskripsi' => $input->post('deskripsi'),
      ];
      $nip = $input->post('nip');
      if(!empty($nip)){
        if(!empty($this->m_kelas->get_row(['id_kelas'=>$where]))){

        }else{
          $params['nip'] = $nip;
          $munas_id = $this->m_jenis->get_row(['jenis_mapel'=>'Muatan Internasional'])['id_jenismapel'];
          $mapel_munas = $this->m_mapel->get(['id_jenismapel'=>$munas_id]);
          for ($i=0; $i < count($mapel_munas); $i++) {
            $params2 = [
              'nip' => $nip,
              'id_mapel' => $mapel_munas['id_mapel']
            ];
            $this->m_pengajar->store($params2);
          }
        }
      }
      $add = $this->m_kelas->update(['id_kelas'=>$where],$params);
      $this->session->set_flashdata('editkelas',$add);
      redirect('kelas');
    }else{
      $this->edit($where);
    }
  }

  public function update_status($where){
    $kelas = $this->m_kelas->get_row(['id_kelas'=>$where]);
    if(count($kelas)>0){
      if($kelas['status'] == '1'){
        $params = ['status' => '0'];
      }else{
        $params = ['status' => '1'];
      }
      $this->m_kelas->update(['id_kelas'=>$where],$params);
      $this->session->set_flashdata('update_status',TRUE);
    }else{
      $this->session->set_flashdata('update_status',FALSE);
    }
    redirect('kelas');
  }
}
