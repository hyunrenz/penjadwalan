<?php
defined("BASEPATH") OR exit("No direct script access allowed");
class Pengumuman extends MY_Controller{
  public function __construct(){
    parent::__construct();
    if(!$this->check_sess()){
      redirect('auth');
    }
  }

  public function form_rules(){
    $form = $this->form_validation;
    $form->set_rules('judul','Judul','required|trim');
    $form->set_rules('pengumuman','Pengumuman','required');
  }

  public function index(){
    $data['_view'] = 'pengumuman/index';
    $data['pengumuman'] = $this->m_pengumuman->all();
    for ($i=0; $i < count($data['pengumuman']); $i++) {
      $data['pengumuman'][$i]['created_at'] = date_indonesian($data['pengumuman'][$i]['created_at']);
    }
    $this->load->view('layout/index',$data);
  }

  public function add(){
    $data['_view'] = 'pengumuman/add';
    $this->load->view('layout/index',$data);
  }

  public function store(){
    $this->form_rules();
    $form = $this->form_validation;
    if($form->run()){
      $params = [
        'judul' => $this->input->post('judul'),
        'pengumuman' => $this->input->post('pengumuman')
      ];
      $add = $this->m_pengumuman->store($params);
      $this->session->set_flashdata('addpengumuman',$add);
      redirect('pengumuman');
    }else{
      $this->add();
    }
  }

  public function edit($where){
    $data['_view'] = 'pengumuman/edit';
    $data['pengumuman'] = $this->m_pengumuman->get_row(['id_pengumuman'=>$where]);
    $this->load->view('layout/index',$data);
  }

  public function update($where){
    $this->form_rules();
    $form = $this->form_validation;
    if($form->run()){
      $params = [
        'judul' => $this->input->post('judul'),
        'pengumuman' => $this->input->post('pengumuman')
      ];
      $edit = $this->m_pengumuman->update(['id_pengumuman'=>$where],$params);
      $this->session->set_flashdata('editpengumuman',$edit);
      redirect('pengumuman');
    }else{
      $this->edit($where);
    }
  }

  public function delete($where){
    $pengumuman = $this->m_pengumuman->get_row(['id_pengumuman'=>$where]);
    if(count($pengumuman) > 0){
      $delete = $this->m_pengumuman->delete(['id_pengumuman'=>$where]);
      $this->session->set_flashdata(['deletepengumuman',$delete]);
    }else{
      $this->session->set_flashdata(['deletepengumuman',FALSE]);
    }
    redirect('pengumuman');
  }
}
