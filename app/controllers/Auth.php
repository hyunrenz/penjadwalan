<?php
defined("BASEPATH") OR exit("No direct script access allowed");
class Auth extends MY_Controller{
  public function __construct(){
    parent::__construct();
  }
  public function index(){
    if($this->check_sess()){
      redirect('pengumuman');
    }
    $this->load->view('layout/login');
  }
  public function login(){
    $username = $this->input->post('username');
    $password = sha1($this->input->post('password'));
    $guru = $this->m_guru->get_row(['username'=>$username,'password'=>$password,'status'=>'1']);
    if(count($guru) > 0){
      $this->session->set_userdata('user',$guru);
      $this->session->set_userdata('base_url',base_url());
      redirect('pengumuman');
    }else{
      $this->session->set_flashdata('auth',FALSE);
      redirect('auth');
    }
  }
  public function logout(){
    $this->session->sess_destroy();
    redirect('auth');
  }
}
