<?php
defined("BASEPATH") OR exit("No direct script access allowed");
class Dashboard extends MY_Controller{
  public function __construct(){
    parent::__construct();

  }

  public function index(){
    $data['_view'] = 'pengumuman/index';
    $this->load->view('layout/index',$data);
  }

  public function add(){
    $data['_view'] = 'pengumuman/add';
    $this->load->view('layout/index',$data);
  }

  public function store($params){
    //TODO: Write your code here
  }

  public function edit($where){
    //TODO: Write your code here
  }

  public function update($where,$params){
    //TODO: Write your code here
  }

  public function delete($where){
    //TODO: Write your code here
  }
}
