<?php
defined("BASEPATH") OR exit("No direct script access allowed");

class Profil extends MY_Controller{

	/**
	 * Profil constructor.
	 */
	public function __construct()
	{
		parent::__construct();
		if(!$this->check_sess()){
			redirect('auth');
		}
	}
	public function form_rules($type){
		$form = $this->form_validation;
		if($type == "add"){
			$form->set_rules('nip','NIP','required|trim|is_unique[guru.username]');
			$form->set_rules('username','Username','required|trim|is_unique[guru.username]');
			$form->set_rules('password','Password','required|trim');
		}else{
			$form->set_rules('nip','NIP','required|trim');
			$form->set_rules('username','Username','required|trim');
			$form->set_rules('password','Password','trim');
		}
		$form->set_rules('nama','Nama','required|trim');
		$form->set_rules('jenis_kelamin','Jenis Kelamin','required|trim');
		$form->set_rules('tanggal_lahir','Tanggal Lahir','required|trim');
		$form->set_rules('email','Email','required|trim|valid_email');
		$form->set_rules('jabatan','Jabatan','required|trim');
		$form->set_rules('tanggal_masuk','Tanggal Masuk','required|trim');
		$form->set_rules('telepon','Telepon','required|trim');
		$form->set_rules('alamat','Alamat','required|trim');
		$form->set_rules('agree','Checkbox','required');
	}
	public function index(){
		$data['_view'] = "profil/index";
		$this->load->view('layout/index',$data);
	}

	public function edit($where){
		$data['_view'] = 'profil/edit';

		$data['guru']['tanggal_lahir'] = date('Y-m-d',strtotime($this->session->userdata('user')['tanggal_lahir']));
		$data['guru']['tanggal_masuk'] = date('Y-m-d',strtotime($this->session->userdata('user')['tanggal_masuk']));
		echo $this->load->view('layout/index',$data,TRUE);
	}

	public function update($where){

		$this->form_rules('edit');
		$form = $this->form_validation;
		$input = $this->input;
		if($form->run()){
			$nip = $this->input->post('nip');
			$username = $this->input->post('username');
			$guru = $this->m_guru->get_row(['nip'=>$where]);
			if($guru['nip'] != $nip){
				$check = $this->m_guru->get_row(['nip'=>$nip]);
				if(!empty($check)){
					$this->session->set_flashdata('nip','<p class="alert alert-danger">the NIP field is already taken.</p>');
					$this->edit($where);
					exit();
				}
			}
			if($guru['username'] != $username){
				$check = $this->m_guru->get_row(['username'=>$username]);
				if(!empty($check)){
					$this->session->set_flashdata('username','<p class="alert alert-danger">the Username field is already taken.</p>');
					$this->edit($where);
					exit();
				}
			}
			if(!empty($_FILES['foto']['name'])){
				$config['upload_path']          = dirname($_SERVER['SCRIPT_FILENAME'])."/assets/foto";
				$config['allowed_types']        = 'jpg|png|gif';
				$config['file_name']						= uuid('foto');
				$config['max_size']             = 10000;
				$config['max_width']            = 5000;
				$config['max_height']           = 5000;
				$config['file_ext_tolower']     = TRUE;
				$config['overwrite']     				= TRUE;
				$upload = $this->upload_file('foto','',[],$config);
				if($upload['success'] != TRUE){
					$this->session->set_flashdata('foto',$upload['message']);
					$this->add();
					exit();
				}
				$foto = $upload['file_name'];
			}
			// if(!empty($_FILES['ijazah']['name'])){
			//   $config['upload_path']          = dirname($_SERVER['SCRIPT_FILENAME'])."/assets/ijazah";
			//   $config['allowed_types']        = 'pdf';
			//   $config['file_name']						= uuid('ijazah');
			//   $config['max_size']             = 10000;
			//   // $config['max_width']            = 5000;
			//   // $config['max_height']           = 5000;
			//   $config['file_ext_tolower']     = TRUE;
			//   $config['overwrite']     				= TRUE;
			//   $upload = $this->upload_file('ijazah','',[],$config);
			//   if($upload['success'] != TRUE){
			//     $this->session->set_flashdata('ijazah',$upload['message']);
			//     $this->add();
			//     exit();
			//   }
			//   $ijazah = $upload['file_name'];
			// }
			$params = [
				'nip' => $nip,
				'nama' => $input->post('nama'),
				'username' => $username,
				'jenis_kelamin' => $input->post('jenis_kelamin'),
				'tanggal_lahir' => $input->post('tanggal_lahir'),
				'email' => $input->post('email'),
				'jabatan' => $input->post('jabatan'),
				'tanggal_masuk' => $input->post('tanggal_masuk'),
				'telepon' => $input->post('telepon'),
				'alamat' => $input->post('alamat'),
				// 'ijazah' => empty($ijazah)?"":$ijazah
			];
			if(!empty($foto)){
				$params['foto'] = $foto;
			}
			if(!empty($password)){
				$params['password'] = sha1($input->post('password'));
			}
			// $this->form_validation->set_message('_check_valid_username', 'Error Message');

			$edit = $this->m_guru->update(['nip'=>$where],$params);

			$this->session->set_flashdata('profil',$edit);

			$new = $this->m_guru->get_row(['username'=>$username,'password'=>$password,'status'=>'1']);
			$this->session->set_userdata('user',$new);
			redirect('profil');

		}else{
			$this->edit($where);
		}

	}

}
?>
