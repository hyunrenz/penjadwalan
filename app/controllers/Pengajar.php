<?php
defined("BASEPATH") OR exit("No direct script access allowed");
class Pengajar extends MY_Controller{
  public function __construct(){
    parent::__construct();
    if(!$this->check_sess()){
      redirect('auth');
    }
  }

  public function form_rules(){
    $form = $this->form_validation;
    $form->set_rules('nip','Pengajar','required|trim');
    $form->set_rules('id_mapel','Mata Pelajaran','required|trim');
  }

  public function index(){
    $data['_view'] = 'pengajar/index';
    // $data['pengajar'] = $this->m_pengajar->get(['status'=>'1']);
    $data['pengajar'] = $this->m_pengajar->all_pengajar();
    $this->load->view('layout/index',$data);
  }

  public function add(){
    $data['_view'] = 'pengajar/add';
    $data['guru'] = $this->m_guru->get(['status'=>'1']);
    $data['mapel'] = $this->m_mapel->get(['status'=>'1']);
    for ($i=0; $i < count($data['mapel']) ; $i++) {
      $data['mapel'][$i]['jenis_mapel'] = $this->m_jenis->get_row(['id_jenismapel'=>$data['mapel'][$i]['id_jenismapel']])['jenis_mapel'];
    }
    $this->load->view('layout/index',$data);
  }

  public function store(){
    $this->form_rules();
    $form = $this->form_validation;
    $input = $this->input;
    if($form->run()){
      $params = [
        'nip' => $input->post('nip'),
        'id_mapel' => $input->post('id_mapel')
      ];
      $add = $this->m_pengajar->store($params);
      $this->session->set_flashdata('addpengajar',$add);
      redirect('pengajar');
    }else{
      $this->add();
    }
  }

  public function edit($where){
    $data['_view'] = 'pengajar/edit';
    $data['guru'] = $this->m_guru->get(['status'=>'1']);
    $data['mapel'] = $this->m_mapel->get(['status'=>'1']);
    $data['pengajar'] = $this->m_pengajar->get_row(['id_pengajar'=>$where]);
    $this->load->view('layout/index',$data);
  }

  public function update($where){
    $this->form_rules();
    $form = $this->form_validation;
    $input = $this->input;
    if($form->run()){
      $params = [
        'nip' => $input->post('nip'),
        'id_mapel' => $input->post('id_mapel')
      ];
      $edit = $this->m_pengajar->update(['id_pengajar'=>$where],$params);
      $this->session->set_flashdata('editpengajar',$edit);
      redirect('pengajar');
    }else{
      $this->edit($where);
    }
  }

  public function update_status($where){
    $pengajar = $this->m_pengajar->get_row(['id_pengajar'=>$where]);
    if(count($pengajar)>0){
      if($pengajar['status'] == '1'){
        $params = ['status' => '0'];
      }else{
        $params = ['status' => '1'];
      }
      $this->m_pengajar->update(['id_pengajar'=>$where],$params);
      $this->session->set_flashdata('update_status',TRUE);
    }else{
      $this->session->set_flashdata('update_status',FALSE);
    }
    redirect('pengajar');
  }
}
