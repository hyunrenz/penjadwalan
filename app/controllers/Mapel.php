<?php
defined("BASEPATH") OR exit("No direct script access allowed");
class Mapel extends MY_Controller{
  public function __construct(){
    parent::__construct();
    if(!$this->check_sess()){
      redirect('auth');
    }
  }
  public function form_rules(){
    $form = $this->form_validation;
    $form->set_rules('nama_mapel','Nama Mata Pelajaran','required|trim');
    $form->set_rules('id_jenismapel','Jenis Mata Pelajaran','required');
  }

  public function index(){
    $data['_view'] = 'mapel/index';
    $data['mapel'] = $this->m_mapel->get(['status'=>'1']);
    for ($i=0; $i < count($data['mapel']) ; $i++) {
      $data['mapel'][$i]['jenis_mapel'] = $this->m_jenis->get_row(['id_jenismapel'=>$data['mapel'][$i]['id_jenismapel']])['jenis_mapel'];
    }
    $this->load->view('layout/index',$data);
  }

  public function add(){
    $data['jenis_mapel'] = $this->m_jenis->get(['status'=>'1']);
    $data['_view'] = 'mapel/add';
    $this->load->view('layout/index',$data);
  }

  public function store(){
    $this->form_rules();
    $form = $this->form_validation;
    $input = $this->input;
    if($form->run()){
      $nama_mapel = $input->post('nama_mapel');
      $id_jenismapel = $input->post('id_jenismapel');
      $mapel = $this->m_mapel->get_row(['nama_mapel'=>$nama_mapel,'id_jenismapel'=>$id_jenismapel,'status'=>'1']);
      if(empty($mapel)){
        $params = [
          'nama_mapel' => $nama_mapel,
          'id_jenismapel' => $id_jenismapel
        ];
        $add = $this->m_mapel->store($params);
        $this->session->set_flashdata('addmapel',$add);
        redirect('mapel');
      }else{
        $this->session->set_flashdata('mapelada',TRUE);
        redirect('mapel/add');
      }
    }else{
      $this->add();
    }
  }

  public function edit($where){
    $data['mapel'] = $this->m_mapel->get_row(['id_mapel'=>$where]);
    $data['jenis_mapel'] = $this->m_jenis->get(['status'=>'1']);
    $data['_view'] = 'mapel/edit';
    $this->load->view('layout/index',$data);
  }

  public function update($where){
    $this->form_rules();
    $form = $this->form_validation;
    $input = $this->input;
    if($form->run()){
      $mapel = $this->m_mapel->get_row(['id_mapel'=>$where]);
      if($mapel['nama_mapel'] != $input->post('nama_mapel') || $mapel['id_jenismapel'] != $input->post('id_jenismapel')){
        $new_mapel = $this->m_mapel->get_row([
          'nama_mapel' => $input->post('nama_mapel'),
          'id_jenismapel' => $input->post('id_jenismapel'),
          'status' => '1'
        ]);
        if(!empty($new_mapel)){
          $this->session->set_flashdata('mapelada',TRUE);
          redirect('mapel/edit/'.$where);
        }
      }
      $params = [
        'nama_mapel' => $input->post('nama_mapel'),
        'id_jenismapel' => $input->post('id_jenismapel')
      ];
      $edit = $this->m_mapel->update(['id_mapel'=>$where],$params);
      $this->session->set_flashdata('editmapel',$edit);
      redirect('mapel');
    }else{
      $this->edit($where);
    }
  }

  public function update_status($where){
    $mapel = $this->m_mapel->get_row(['id_mapel'=>$where]);
    if(count($mapel)>0){
      if($mapel['status'] == '1'){
        $params = ['status' => '0'];
      }else{
        $params = ['status' => '1'];
      }
      $this->m_mapel->update(['id_mapel'=>$where],$params);
      $this->session->set_flashdata('update_status',TRUE);
    }else{
      $this->session->set_flashdata('update_status',FALSE);
    }
    redirect('mapel');
  }
}
