<?php
defined("BASEPATH") OR exit("No direct script access allowed");
class Waktu extends MY_Controller{
  public function __construct(){
    parent::__construct();
    if(!$this->check_sess()){
      redirect('auth');
    }
  }

  public function form_rules(){
    $form = $this->form_validation;
    $form->set_rules('jenis_kegiatan','Jenis Kegiatan','required|trim');
    $form->set_rules('kegiatan','Kegiatan','required|trim');
    $form->set_rules('jenis_waktu','Jenis Waktu','required|trim');
    $form->set_rules('jam_mulai','Jam Mulai','required|trim|callback_valid_time',[
      'valid_time' => "%s Cannot be greater than Jam Selesai"
    ]);
    $form->set_rules('jam_selesai','Jam Selesai','required|trim|callback_valid_time',[
      'valid_time' => "%s Cannot be smaller than Jam Mulai"
    ]);
  }

  public function valid_time($str){
    $jam_mulai = $this->input->post('jam_mulai');
    $jam_selesai = $this->input->post('jam_selesai');
    return strtotime($jam_mulai) < strtotime($jam_selesai);
  }

  public function index(){
    $data['_view'] = 'waktu/index';
    $data['waktu'] = $this->m_waktu->get(['status'=>'1']);
    $this->load->view('layout/index',$data);
  }

  public function add(){
    $data['_view'] = 'waktu/add';
    $this->load->view('layout/index',$data);
  }

  public function store(){
    $this->form_rules();
    $form = $this->form_validation;
    $input = $this->input;
    if($form->run()){
      $waktu = $this->m_waktu->get_row([
        'jenis_kegiatan' => $input->post('jenis_kegiatan'),
        'jenis_waktu' => $input->post('jenis_waktu'),
        'jam_mulai' => $input->post('jam_mulai'),
        'jam_selesai' => $input->post('jam_selesai')
      ]);
      if(empty($waktu)){
        $params = [
          'jenis_kegiatan' => $input->post('jenis_kegiatan'),
          'kegiatan' => $input->post('kegiatan'),
          'jenis_waktu' => $input->post('jenis_waktu'),
          'jam_mulai' => $input->post('jam_mulai'),
          'jam_selesai' => $input->post('jam_selesai')
        ];
        $add = $this->m_waktu->store($params);
        $this->session->set_flashdata('addwaktu',$add);
        redirect('waktu');
      }else{
        $this->session->set_flashdata('waktuada',TRUE);
        redirect('waktu/add');
      }
    }else{
      $this->add();
    }
  }

  public function edit($where){
    $data['_view'] = 'waktu/edit';
    $data['waktu'] = $this->m_waktu->get_row(['id_waktu'=>$where]);
    $this->load->view('layout/index',$data);
  }

  public function update($where){
    $this->form_rules();
    $form = $this->form_validation;
    $input = $this->input;
    if($form->run()){
      $waktu = $this->m_waktu->get_row(['id_waktu'=>$where]);
      if($waktu['jenis_kegiatan'] != $input->post('jenis_kegiatan') || $waktu['jenis_waktu'] != $input->post('jenis_waktu') || strtotime($waktu['jam_mulai']) != strtotime($input->post('jam_mulai')) || strtotime($waktu['jam_selesai']) != strtotime($input->post('jam_selesai'))){
        $new_waktu = $this->m_waktu->get_row([
          'jenis_kegiatan' => $input->post('jenis_kegiatan'),
          'jenis_waktu' => $input->post('jenis_waktu'),
          'jam_mulai' => $input->post('jam_mulai'),
          'jam_selesai' => $input->post('jam_selesai')
        ]);
        if(!empty($new_waktu)){
          $this->session->set_flashdata('waktuada',TRUE);
          redirect('waktu/edit/'.$where);
        }
      }
      $params = [
        'jenis_kegiatan' => $input->post('jenis_kegiatan'),
        'kegiatan' => $input->post('kegiatan'),
        'jenis_waktu' => $input->post('jenis_waktu'),
        'jam_mulai' => $input->post('jam_mulai'),
        'jam_selesai' => $input->post('jam_selesai')
      ];
      $edit = $this->m_waktu->update(['id_waktu'=>$where],$params);
      $this->session->set_flashdata('editwaktu',$edit);
      redirect('waktu');
    }else{
      $this->edit($where);
    }
  }

  public function update_status($where){
    $waktu = $this->m_waktu->get_row(['id_waktu'=>$where]);
    if(count($waktu)>0){
      if($waktu['status'] == '1'){
        $params = ['status' => '0'];
      }else{
        $params = ['status' => '1'];
      }
      $this->m_waktu->update(['id_waktu'=>$where],$params);
      $this->session->set_flashdata('update_status',TRUE);
    }else{
      $this->session->set_flashdata('update_status',FALSE);
    }
    redirect('waktu');
  }
}
