<?php
defined("BASEPATH") OR exit("No direct script access allowed");
class Jenis extends MY_Controller{
  public function __construct(){
    parent::__construct();
    if(!$this->check_sess()){
      redirect('auth');
    }
  }
  public function form_rules(){
    $form = $this->form_validation;
    $form->set_rules('jenis_mapel','Jenis Mata Pelajaran','required|trim');
  }

  public function index(){
    $data['_view'] = 'jenis/index';
    $data['jenis'] = $this->m_jenis->get(['status'=>'1']);
    $this->load->view('layout/index',$data);
  }

  public function store(){
    $this->form_rules();
    $form = $this->form_validation;
    $input = $this->input;
    if($form->run()){
      $params = [
        'jenis_mapel' => $input->post('jenis_mapel')
      ];
      $add = $this->m_jenis->store($params);
      $this->session->set_flashdata('addjenis',$add);
      redirect('jenis');
    }else{
      $this->index();
    }
  }

  public function edit($where){
    $data['jenis'] = $this->m_jenis->get_row(['id_jenismapel'=>$where]);
    $data['_view'] = 'jenis/edit';
    $this->load->view('layout/index',$data);
  }

  public function update($where){
    $this->form_rules();
    $form = $this->form_validation;
    $input = $this->input;
    if($form->run()){
      $params = [
        'jenis_mapel' => $input->post('jenis_mapel')
      ];
      $add = $this->m_jenis->update(['id_jenismapel'=>$where],$params);
      $this->session->set_flashdata('editjenis',$add);
      redirect('jenis');
    }else{
      $this->edit($where);
    }
  }

  public function update_status($where){
    $jenis = $this->m_jenis->get_row(['id_jenismapel'=>$where]);
    if(count($jenis)>0){
      if($jenis['status'] == '1'){
        $params = ['status' => '0'];
      }else{
        $params = ['status' => '1'];
      }
      $this->m_jenis->update(['id_jenismapel'=>$where],$params);
      $this->session->set_flashdata('update_status',TRUE);
    }else{
      $this->session->set_flashdata('update_status',FALSE);
    }
    redirect('jenis');
  }
}
