<?php defined('BASEPATH') OR exit('No direct script access allowed');
// MY HELPER , Joseph Alberto <hyunrenz@gmail.com>

/**
* Nothing to explain
*/
function ci(){
  return get_instance();
}
/**
* UUID function
*
* this function is working for making unique string
*
* @param $code
* @return string
* @category public function
*
*/
function uuid($code = ''){
  ci()->load->library('uuid');
  $str = ci()->uuid->v5(hex2bin('c4881179a92a742441fd5b51f6ca85d2'));
  if($code != ''){
    return $code."-".ci()->uuid->v5($str.$str);
  }else{
    return ci()->uuid->v5($str.$str);
  }
}
/**
* Date Indonesian Function
*
* this function is working for change the date that given from parameter,
* into indonesian date.
*
* @param $date
* @return string
* @category public function
*
*/
function date_indonesian($date){
  $month = DATE('m',strtotime($date));
  switch($month){
    case '1': $month = "Januari";break;
    case '2': $month = "Februari";break;
    case '3': $month = "Maret";break;
    case '4': $month = "April";break;
    case '5': $month = "Mei";break;
    case '6': $month = "Juni";break;
    case '7': $month = "Juli";break;
    case '8': $month = "Agustus";break;
    case '9': $month = "September";break;
    case '10': $month = "Oktober";break;
    case '11': $month = "November";break;
    case '12': $month = "Desember";break;
  }
  return DATE('d',strtotime($date)).' '.$month.' '.DATE('Y',strtotime($date));
}
/**
* Encrypt function
*
* this function will return the encrypt string from the
* value that given to this function
*
* @param $str
* @return string
* @category public function
*
*/
function enc($str){
  ci()->load->library('encryption');
  return str_replace('/','t3mpsl4sh',ci()->encryption->encrypt($str));
}

/**
* Decrypt function
*
* this function will return the original string from the
* encrypt value that given to this function
*
* @param $str
* @return string
* @category public function
*
*/
function dec($str){
  ci()->load->library('encryption');
  return ci()->encryption->decrypt(str_replace('t3mpsl4sh','/',$str));
}

/* JSON ENCODE & JSON DECODE */
function json_e($data = NULL){
  header('Content-Type: application/json');
  exit(json_encode($data));
}
function json_d($data = NULL){
  header('Content-Type: application/json');
  exit(json_decode($data));
}
/* END JSON ENCODE & JSON DECODE */

/* CSRF Token & Name */
function get_csrf_token(){
  return $this->security->get_csrf_hash();
}
function get_csrf_name(){
  return $this->security->get_csrf_token_name();
}
/* End CSRF Token & Name */
