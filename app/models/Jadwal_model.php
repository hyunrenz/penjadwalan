<?php
defined("BASEPATH") OR exit("No direct script access allowed");
class Jadwal_model extends MY_Model{
  protected $table = 'jadwal';
  public function jadwal_kelas($kelas,$tahun){
    $query = "SELECT j.id_jadwal,j.hari,w.kegiatan,w.jam_mulai,w.jam_selesai,m.nama_mapel,k.kelas,g.nama
              FROM jadwal j
              LEFT JOIN guru g ON j.nip = g.nip
              LEFT JOIN mapel m ON j.id_mapel = m.id_mapel
              JOIN kelas k ON j.id_kelas = k.id_kelas
              JOIN waktu w ON j.id_waktu = w.id_waktu
              WHERE j.id_kelas = '$kelas'
              AND j.tahun_ajaran = '$tahun'
              ORDER BY j.hari ASC,w.jam_mulai ASC
    ";
    return $this->db->query($query)->result_array();
  }
  public function jadwal_guru($nip,$tahun){
    $query = "SELECT *
              FROM jadwal j
              LEFT JOIN guru g ON j.nip = g.nip
              LEFT JOIN mapel m ON j.id_mapel = m.id_mapel
              JOIN kelas k ON j.id_kelas = k.id_kelas
              JOIN waktu w ON j.id_waktu = w.id_waktu
              WHERE j.nip = '$nip'
              AND j.tahun_ajaran = $tahun
              AND j.status = '1'
              ORDER BY j.hari ASC, w.jam_mulai ASC
    ";
    return $this->db->query($query)->result_array();
  }
  public function group_tahun(){
    $query = "SELECT tahun_ajaran tahun FROM jadwal WHERE status = '1' GROUP BY tahun_ajaran ORDER BY tahun_ajaran DESC";
    return $this->db->query($query)->result_array();
  }
}
