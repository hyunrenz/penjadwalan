<?php
defined("BASEPATH") OR exit("No direct script access allowed");
class Pengajar_model extends MY_Model{
  protected $table = 'pengajar';
  public function all_pengajar(){
    $sql = "SELECT * FROM pengajar p LEFT JOIN guru g ON p.nip = g.nip LEFT JOIN mapel m ON p.id_mapel = m.id_mapel LEFT JOIN jenis_mapel j ON m.id_jenismapel = j.id_jenismapel WHERE p.status = '1'";
    return $this->db->query($sql)->result_array();
  }
}
