<?php defined('BASEPATH') or exit('No direct script access allowed');
/**
* > MY Model class
*
* this is my own modification class, in which there are many functions
* that help me to develop a web so that it easier to access the database
*
* @version 1.0.0
*
*/
class MY_Model extends CI_Model
{
  /**
  * params attribute
  *
  * This parameter is useful for storing data that will be used
  * for several database operations such as insert, and updates
  *
  * @category public attr
  * @var array
  *
  */
  public $params = array();
  /**
  * select attribute
  *
  * This parameter is useful for filtering some fields that
  * will be retrieved by the data
  *
  * @category public attr
  * @var string
  *
  */
  public $select = "";
  /**
  * All function
  *
  * this function is useful for retrieving all rows in a particular table
  *
  * @param $limit limiting row
  * @param $offset start from particular row
  * @param $order ordering data by certain column
  * @category public function
  * @return array
  *
  */
  public function all($limit = "", $offset = "", $order = "ASC")
  {
    $db = $this->db;
    if ($this->select != "") {
      $db->select($this->select);
      $this->select = "";
    }
    if ($limit != "") {
      $db->limit($limit);
    }
    if ($offset != "") {
      $db->offset($offset);
    }
    if (is_array($order)) {
      $field = array_keys($order)[0];
      $db->order_by($field, $order[$field]);
    } else {
      $db->order_by('created_at', $order);
    }

    return $db->get($this->table)->result_array();
  }

  /**
  * Get function
  *
  * this function is work for retrieving several rows in particular table
  *
  * @param $where the field and value to be compared
  * @param $limit limiting row
  * @param $offset start from particular row
  * @param $order ordering data by certain column
  * @category public function
  * @return array
  *
  */
  public function get($where, $limit = "", $offset = "", $order = "ASC")
  {
    $db = $this->db;
    if ($this->select != "") {
      $db->select($this->select);
      $this->select = "";
    }
    if (is_array($where)) {
      for ($i = 0; $i < count($where); $i++) {
        $field = array_keys($where)[$i];
        $db->where($field, $where[$field]);
      }
    } else {
      $db->where($where);
    }
    if ($limit != "") {
      $db->limit($limit);
    }
    if ($offset != "") {
      $db->offset($offset);
    }
    if (is_array($order)) {
      $field = array_keys($order)[0];
      $db->order_by($field, $order[$field]);
    } else {
      $db->order_by('created_at', $order);
    }

    return $db->get($this->table)->result_array();
  }

  /**
  * Get row function
  *
  * this function is working for retrieving only one row in particular table
  *
  * @param $where the field and value to be compared
  * @category public function
  * @return array
  *
  */
  public function get_row($where)
  {
    $db = $this->db;
    if ($this->select != "") {
      $db->select($this->select);
      $this->select = "";
    }
    if (is_array($where)) {
      for ($i = 0; $i < count($where); $i++) {
        $field = array_keys($where)[$i];
        $db->where($field, $where[$field]);
      }
    } else {
      $db->where($where);
    }
    return $db->get($this->table)->row_array();
  }

  /**
  * Store function
  *
  * this function is working for inserting value into particular table
  *
  * @param $arr
  * @category public function
  * @see $params::MY_Model attribute
  * @return bool
  *
  */
  public function store($arr = NULL)
  {
    $db = $this->db;
    if ($arr != NULL) {
      return $db->insert($this->table, $arr);
    } else {
      return $db->insert($this->table, $this->params);
    }
  }

  /**
  * Update function
  *
  * this function is working for updating value from particular table
  *
  * @param $arr
  * @param $where the field and value to be compared
  * @category public function
  * @see $params::MY_Model attribute
  * @return bool
  *
  */
  public function update($where, $arr = NULL)
  {
    $db = $this->db;
    if (is_array($where)) {
      for ($i = 0; $i < count($where); $i++) {
        $field = array_keys($where)[$i];
        $db->where($field, $where[$field]);
      }
    } else {
      $db->where($where);
    }
    if ($arr != NULL) {
      return $db->update($this->table, $arr);
    } else {
      return $db->update($this->table, $this->params);
    }
  }

  /**
  * Delete function
  *
  * this function is working for deleting row from particular table
  *
  * @param $where the field and value to be compared
  * @category public function
  * @return bool
  *
  */
  public function delete($where)
  {
    $db = $this->db;
    if (is_array($where)) {
      for ($i = 0; $i < count($where); $i++) {
        $field = array_keys($where)[$i];
        $db->where($field, $where[$field]);
      }
    } else {
      $db->where($where);
    }
    return $db->delete($this->table);
  }

  /**
  * Count row function
  *
  * this function is very helpfull for counting total of row from particular table
  *
  * @param $where
  * @category public function
  * @return int
  *
  **/
  public function count_row($where = '')
  {
    $db = $this->db;
    if ($where != '') {
      if (is_array($where)) {
        for ($i = 0; $i < count($where); $i++) {
          $field = array_keys($where)[$i];
          $db->where($field, $where[$field]);
        }
      } else {
        $db->where($where);
      }
    }
    return $db->get($this->table)->num_rows();
  }

  public function datenow(){
    $db = $this->db;
    $db->select('NOW() AS datenow');
    return $db->get()->row('datenow');
  }
}
