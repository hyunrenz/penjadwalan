<?php defined('BASEPATH') OR exit('No direct script access allowed');
/**
* > MY Controller
*
* @version 1.0.0
*
*/
class MY_Controller extends CI_Controller{
  public function __construct(){
    parent::__construct();
    $load = $this->load;
    $load->model('pengumuman_model','m_pengumuman');
    $load->model('guru_model','m_guru');
    $load->model('kelas_model','m_kelas');
    $load->model('mapel_model','m_mapel');
    $load->model('jenismapel_model','m_jenis');
    $load->model('waktu_model','m_waktu');
    $load->model('pengajar_model','m_pengajar');
    $load->model('jadwal_model','m_jadwal');
  }
  /**
  * Mutliple Upload Function
  *
  * this function is working for uploading images
  *
  * @param $field,$path,$resize
  * @return array
  * @category public function
  *
  */
  public function multiple_upload($field,$position,$path,$resize=[]){
    $_FILES['file']['name'] = $_FILES[$field]['name'][$position];
    $_FILES['file']['type'] = $_FILES[$field]['type'][$position];
    $_FILES['file']['tmp_name'] = $_FILES[$field]['tmp_name'][$position];
    $_FILES['file']['error'] = $_FILES[$field]['error'][$position];
    $_FILES['file']['size'] = $_FILES[$field]['size'][$position];
    $status = $this->upload_image('file',$path,$resize);
    return $status;
  }

  /**
  * Uplaod file function
  *
  * this function is working for uploading files
  *
  * @param $field,$path,$resize
  * @return array
  * @category public function
  *
  */
  public function upload_file($field,$path,$resize=array(),$config="",$name=""){
    if($config == ""){
      $config['upload_path']          = dirname($_SERVER['SCRIPT_FILENAME'])."/".$path;
      $config['allowed_types']        = '*';
      $config['file_name']						= $name != "" ? $name : uuid('uploadfile');
      $config['max_size']             = 10000;
      /* this configuration only for images */
      // $config['max_width']            = 5000;
      // $config['max_height']           = 5000;
      $config['file_ext_tolower']     = TRUE;
      $config['overwrite']     				= TRUE;
    }
    $this->load->library('upload', $config);
    $this->upload->initialize($config);
    $result['success'] = $this->upload->do_upload($field);
    if($result['success']){
      $result['file_name'] = $this->upload->data()['file_name'];
      return (!count($resize)>0)? $result : $this->resize_image($path,$result['file_name'],$resize) ;
    }else{
      $result['message'] = $this->upload->display_errors();
      return $result;
    }
  }

  /**
  * Resize Image Function
  *
  * this function is working for resizing images,
  * and this function only called when developer given third parameter
  * into Upload images functoin
  *
  * @param $field,$path,$resize
  * @see upload_image::MY_Controller function
  * @return array
  * @category public function
  *
  */
  public function resize_image($path,$filename,$resize){
    $config['image_library'] 			= 'gd2';
    $config['source_image'] 			= dirname($_SERVER['SCRIPT_FILENAME'])."/".$path.'/'.$filename;
    $config['create_thumb'] 			= FALSE;
    $config['file_permissions'] 	= 0644;
    $config['quality'] 						= "100%";
    $config['maintain_ratio'] 		= FALSE;
    $config['width']         			= $resize[0];
    $config['height']       			= $resize[1];
    $config['new_image'] 					= dirname($_SERVER['SCRIPT_FILENAME'])."/".$path.'/thumb_'.$filename;

    $this->load->library('image_lib', $config);
    $result['success'] = $this->image_lib->resize();
    $result['file_name'] = $filename;
    return $result;
  }

  /**
  * Check Session Function
  *
  * this function is working for checking session,
  * and it also checking the orginality of the user account
  *
  * @return bool
  * @category public function
  *
  */
  public function check_sess(){
    $sess = $this->session;
    if($sess->has_userdata('user') && $sess->userdata('base_url') == base_url()){
      $user_on_session = $this->m_guru->get_row(['nip'=>$sess->userdata('user')['nip']]);
      return !empty($user_on_session);
    }else{
      return FALSE;
    }
  }

  /**
  * Check Level Function
  *
  * If you have a level table in your project
  * this function might be will very useful for helping you to check user level
  *
  * @param $allowedLevel
  * @return bool $result
  * @category public function
  *
  */
  public function check_level($allowedLevel=""){
    $level = $this->session->userdata('user')['level_id'];
    $user_level = strtolower($this->m_level->get_row(['level_id'=>$level])['level']);
    if(is_array($allowedLevel)){
      $result = FALSE;
      foreach($allowedLevel as $al){
        $result = $result || ($al == $user_level);
      }
    }else{
      $result = ($allowedLevel == $user_level);
    }
    return $result;
  }

  /**
  * Pagination Function
  *
  * this function is for helping you making pagination
  *
  * @param $url,$perpage,$uri,$totalrow
  * @return array $return_data
  * @category public function
  *
  */
  public function paginate($url,$perpage,$uri,$totalrow){
    /* Config */
    $config['base_url'] = site_url($url); //site url
    $config['total_rows'] = $totalrow; //total row
    $config['per_page'] = $perpage;  //show record per halaman
    $config["uri_segment"] = $uri;  // uri parameter
    $choice = $config["total_rows"] / $config["per_page"];
    $config["num_links"] = floor($choice);

    /* Style Pagination */
    $config['first_link']       = 'First';
    $config['last_link']        = 'Last';
    $config['next_link']        = 'Next';
    $config['prev_link']        = 'Prev';
    $config['full_tag_open']    = '<div class="pagging text-center"><nav><ul class="pagination justify-content-center">';
    $config['full_tag_close']   = '</ul></nav></div>';
    $config['num_tag_open']     = '<li class="page-item"><span class="page-link">';
    $config['num_tag_close']    = '</span></li>';
    $config['cur_tag_open']     = '<li class="page-item active"><span class="page-link">';
    $config['cur_tag_close']    = '<span class="sr-only">(current)</span></span></li>';
    $config['next_tag_open']    = '<li class="page-item"><span class="page-link">';
    $config['next_tagl_close']  = '<span aria-hidden="true">&raquo;</span></span></li>';
    $config['prev_tag_open']    = '<li class="page-item"><span class="page-link">';
    $config['prev_tagl_close']  = '</span>Next</li>';
    $config['first_tag_open']   = '<li class="page-item"><span class="page-link">';
    $config['first_tagl_close'] = '</span></li>';
    $config['last_tag_open']    = '<li class="page-item"><span class="page-link">';
    $config['last_tagl_close']  = '</span></li>';

    $this->pagination->initialize($config);

    $data['page'] = ($this->uri->segment($uri)) ? $this->uri->segment($uri) : 0;
    $data['pagination'] = $this->pagination->create_links();

    return $return_data = [$data,$config];
  }
}
